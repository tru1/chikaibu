from lib.helpers import *


class LibraryMenu(QWidget):
    def __init__(self, parent, content_frame):
        super(LibraryMenu, self).__init__(parent)
        library_menu_layout = QHBoxLayout(self)
        library_menu_layout.setAlignment(Qt.AlignVCenter)
        library_menu_layout.setContentsMargins(50, 20, 50, 15)
        add_elem_button = MenuElement(self, Helpers.resource_path("addElem.png"), "Add Element")
        add_elem_button.clicked.connect(lambda: parent.switch(page_id["add_element"], None))
        library_menu_layout.addWidget(add_elem_button)
        library_menu_layout.addSpacing(15)
        bib_file_button = MenuElement(self, Helpers.resource_path("bib_file.png"), "Create .bib")
        bib_file_button.clicked.connect(lambda: self.bib_to_file(content_frame.get_current_query))
        library_menu_layout.addWidget(bib_file_button)
        library_menu_layout.addSpacing(15)
        bib_clip_button = MenuElement(self, Helpers.resource_path("bib_clip.png"), ".bib -> Clip")
        bib_clip_button.clicked.connect(lambda: self.bib_to_clipboard(content_frame.get_current_query))
        library_menu_layout.addWidget(bib_clip_button)
        library_menu_layout.addSpacing(15)
        folder_button = MenuElement(self, Helpers.resource_path("folder.png"), "Open folder")
        folder_button.clicked.connect(lambda: FileHandler().open_file() )
        library_menu_layout.addWidget(folder_button)
        library_menu_layout.addSpacing(15)

    def bib_to_clipboard(self, data_func):
        element_list = data_func()
        bibtex = BibFile().generate(element_list)
        FileHandler().copy_to_clipboard(bibtex)

    def bib_to_file(self, data_func):
        element_list = data_func()
        bibtex = BibFile().generate(element_list)
        FileHandler().copy_to_file(bibtex)


class SettingsMenu(QWidget):
    def __init__(self, parent):
        super(SettingsMenu, self).__init__(parent)
        settings_menu_layout = QHBoxLayout(self)
        settings_menu_layout.setAlignment(Qt.AlignVCenter)
        settings_menu_layout.setContentsMargins(50, 20, 50, 15)
        back_button = MenuElement(self, Helpers.resource_path("back.png"), "Back")
        back_button.clicked.connect(lambda: parent.switch(page_id["library"], None))
        settings_menu_layout.addWidget(back_button)
        settings_menu_layout.addSpacing(15)


class ViewMenu(QWidget):
    def __init__(self, parent, content_frame):
        super(ViewMenu, self).__init__(parent)
        view_menu_layout = QHBoxLayout(self)
        view_menu_layout.setAlignment(Qt.AlignVCenter)
        view_menu_layout.setContentsMargins(50, 20, 50, 15)
        back_button = MenuElement(self, Helpers.resource_path("back.png"), "Back")
        back_button.clicked.connect(lambda: parent.switch(page_id["library"], None))
        view_menu_layout.addWidget(back_button)
        view_menu_layout.addSpacing(15)
        edit_button = MenuElement(self, Helpers.resource_path("edit.png"), "Edit")
        edit_button.clicked.connect(lambda: self.edit_element(content_frame.get_data, parent.switch))
        view_menu_layout.addWidget(edit_button)
        view_menu_layout.addSpacing(15)
        delete_button = MenuElement(self, Helpers.resource_path("delElem.png"), "Delete")
        delete_button.clicked.connect(lambda: self.delete_entry(content_frame.get_data, parent.switch))
        view_menu_layout.addWidget(delete_button)
        view_menu_layout.addSpacing(15)
        bib_button = MenuElement(self, Helpers.resource_path("bib_clip.png"), "Get Bibtex")
        bib_button.clicked.connect(lambda: self.get_bib_data(content_frame.get_data))
        view_menu_layout.addWidget(bib_button)
        view_menu_layout.addSpacing(15)
        link_button = MenuElement(self, Helpers.resource_path("link.png"), "File Link")
        link_button.clicked.connect(lambda: self.return_link(content_frame.get_data))
        view_menu_layout.addWidget(link_button)
        view_menu_layout.addSpacing(15)
        open_button = MenuElement(self, Helpers.resource_path("pdf.png"), "Open PDF")
        open_button.clicked.connect(lambda: self.open_element(content_frame.get_data))
        view_menu_layout.addWidget(open_button)
        view_menu_layout.addSpacing(15)

    def open_element(self, data_func):
        element_data = data_func()
        try:
            FileHandler().open_file(element_data)
        except:
            print("ERROR: Requested file not found")

    def return_link(self, data_func):
        element_data = data_func()
        FileHandler().get_link(element_data)

    def edit_element(self, data_func, switch_func):
        element_data = data_func()
        switch_func(page_id["edit_element"], element_data[DBStructure.ID.id])

    def get_bib_data(self, data_func):
        element_list = [data_func()]
        bibtex = BibFile().generate(element_list)
        FileHandler().copy_to_clipboard(bibtex)

    def delete_entry(self, data_func, switch_func):
        box = QMessageBox()
        box.setIcon(QMessageBox.Question)
        box.setWindowTitle("Delete File")
        box.setText("Are you sure to delete this entry?\nFile and DataBase entries will be deleted!")
        box.addButton(QMessageBox.Yes)
        box.addButton(QMessageBox.Cancel)
        if box.exec_() == QMessageBox.Yes:
            dataset = data_func()
            DBHandler().delete_entry( "documents", {"ID": str(dataset[DBStructure.ID.id])} )
            DBHandler().delete_entry( "documentKeywords", {"documentID": str(dataset[DBStructure.ID.id])} )
            FileHandler().delete_file(dataset)
            switch_func(page_id["library"], None)
        else:
            return


class AddMenu(QWidget):
    file_path = None

    def __init__(self, parent, content_widget):
        super(AddMenu, self).__init__(parent)
        self.parent = parent
        self.content_widget = content_widget
        add_menu_layout = QHBoxLayout(self)
        add_menu_layout.setAlignment(Qt.AlignVCenter)
        add_menu_layout.setContentsMargins(50, 20, 50, 15)
        back_button = MenuElement(self, Helpers.resource_path("back.png"), "Back")
        back_button.clicked.connect(lambda: self.parent.switch(page_id["library"], None))
        add_menu_layout.addWidget(back_button)
        add_menu_layout.addSpacing(15)

        save_button = MenuElement(self, Helpers.resource_path("save.png"), "Save")
        save_button.clicked.connect(lambda: self.save_data())
        add_menu_layout.addWidget(save_button)
        add_menu_layout.addSpacing(30)
        self.drop_file_widget = DropZone(self)
        add_menu_layout.addWidget(self.drop_file_widget)
        add_menu_layout.addSpacing(15)

    def refresh(self):
        self.drop_file_widget.pageIndexChanged()

    def save_data(self):
        path = self.drop_file_widget.get_file_path()
        success = self.content_widget.save_data(path)
        if success is True:
            self.parent.switch(page_id["library"], None)
            self.parent.reset_lib_view()


class EditMenu(QWidget):
    def __init__(self, parent, content_widget):
        super(EditMenu, self).__init__(parent)
        edit_menu_layout = QHBoxLayout(self)
        edit_menu_layout.setAlignment(Qt.AlignVCenter)
        back_button = MenuElement(self, Helpers.resource_path("back.png"), "Back")
        back_button.clicked.connect(lambda: parent.switch(page_id["view_element"], content_widget.get_id()))
        edit_menu_layout.addWidget(back_button)
        edit_menu_layout.addSpacing(15)
        save_button = MenuElement(self, Helpers.resource_path("save.png"), "Save")
        save_button.clicked.connect(lambda: self.save_dat(content_widget.get_id, content_widget.save_data, parent.switch))
        edit_menu_layout.addWidget(save_button)
        edit_menu_layout.addSpacing(15)

    def save_dat(self, data_func, content_save_func, switch_func):
        success = content_save_func()
        id_data = data_func()
        if success is True:
            switch_func(page_id["view_element"], id_data)


class MenuElement(QPushButton):
    def __init__(self, parent, image, text):
        super(MenuElement, self).__init__(parent)
        self.setFixedWidth(100)
        self.setFixedHeight(100)
        menu_element_layout = QVBoxLayout(self)
        menu_element_layout.setContentsMargins(10, 10, 10, 10)
        button = QLabel(self)
        button_pix = Helpers.changeImageColor(QImage(image), Colors.text_medium)
        button.setPixmap(button_pix)
        button.setFixedWidth(80)
        button.setFixedHeight(60)
        button.setAlignment(Qt.AlignCenter)
        button.setStyleSheet("background-color: transparent;")
        menu_element_layout.addWidget(button)
        label = CustomLabel(self, Colors.text_medium, 8, True, text)
        label.setFixedWidth(80)
        label.setFixedHeight(20)
        label.setAlignment(Qt.AlignCenter)
        menu_element_layout.addWidget(label)

