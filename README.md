# Chikaibu

Python-based GUI tool to organise and store academic literature, tested on Windows 10 and Ubuntu 20.04.
> The tool is working but in an early stage without full test coverage, so as always regular backups are advisable.


## Motivation

Besides learning Python this project aims to become a comfortable and customisable way for organising literature, giving the user the posibility to adapt it and select the location for file storage for easier backup.


## Dependencies

- PySide2 (>=5.14)
- pyperclip
- crossref_commons
- arxiv


## Features

- download meta-data from CrossRef and arXiv
- select folder to store pdf files
- keyword system for organisation of literature
- generation of .bib files


## Installation & First steps

- for installation clone the project and install the dependencies or download a freezed executable
- upon first startup a .ini as well as subfolder for literature storage are created at the location of the executable
- new .pdfs can be added by drag&drop (.pdf may not be used in another program)


## Tests

Test are performed using `pytest`, extension of test coverage is in progress and currently only convers
- `lib/config.py`


## License