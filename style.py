from lib.helpers import Colors, Helpers


stylesheet = """

/*::::::: Scroll-Bar :::::::*/
  QScrollBar:vertical {
    border: none;
    width: 10px;
  }

  QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
  }

  QScrollBar::handle:vertical {
    background-color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

  QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
    border: none;
    background: none;
  }

/*::::::: Drop-Down Box :::::::*/
  QComboBox {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border-style: solid;
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    border-radius: 5px;
  }

  QComboBox QAbstractItemView {
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    selection-background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    selection-color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
  }

  QComboBox::drop-down {
    border-left-width: 2px;
    border-left-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    border-left-style: solid;
    border-top-right-radius: 2px;
    border-bottom-right-radius: 2px;
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
  }

/*::::::: Tool-Tips :::::::*/
  QToolTip {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
    border: 0px;
    font-size: 14px;
  }

/*::::::: Progress-Bars :::::::*/
  QProgressBar{
    border: 0px solid;
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    text-align: center;
    color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

  QProgressBar::chunk {
    background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
  }

/*::::::: Pop-Up Messages :::::::*/
  QMessageBox {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
  }

  QMessageBox QLabel {
    color: rgb("""+str(Colors.str_from_color(Colors.text_medium))+""");
    font: 14px;
  }

  QMessageBox QPushButton {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    font: bold 16px;
  }

/*::::::: Line-Edit :::::::*/
  QLineEdit {
    color: rgb("""+str(Colors.str_from_color(Colors.text_medium))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    selection-background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    selection-color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

/*::::::: Text-Edit :::::::*/
  QTextEdit {
    color: rgb("""+str(Colors.str_from_color(Colors.text_medium))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    selection-background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    selection-color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

  QTextEdit#view {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 0px solid;
    selection-background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    selection-color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

/*::::::: Push-Buttons :::::::*/
  QPushButton {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 0px;
    border-radius: 10px;
  }

  QPushButton:pressed {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    border: 0px;
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
  }

  QPushButton#bulk {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    border-radius: 10px;
  }

  QPushButton#bulk:pressed {
    color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
  }

  QPushButton#bulk:disabled {
    color: rgb("""+str(Colors.str_from_color(Colors.text_medium))+""");
    border: 0px;
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
  }

  QPushButton#addDelete {
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 0px;
  }

/*::::::: Frame for keyword label :::::::*/
  QFrame#KeywordElement {
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.dp08))+""");
    border-radius: 5px;
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
  }

/*::::::: Label :::::::*/
  QLabel {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.text_high))+""");
    selection-background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    selection-color: rgb("""+str(Colors.str_from_color(Colors.text_disabled))+""");
  }

  QLabel#keyword {
    color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 0px;
  }
  
/*::::::: Radio-Buttons :::::::*/
  QRadioButton::indicator:checked {
    border: 0px solid;
    width: 0px;
    height: 0px;
  }

  QRadioButton::indicator:unchecked {
    border: 0px solid;
    width: 0px;
    height: 0px;
  }

  QRadioButton#left_button:checked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
  }

  QRadioButton#left_button:unchecked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
  }

  QRadioButton#mid_button:checked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    }

  QRadioButton#mid_button:unchecked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
  }

  QRadioButton#right_button:checked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
  }

  QRadioButton#right_button:unchecked {
    background-color: rgb("""+str(Colors.str_from_color(Colors.dp01))+""");
    border: 2px solid rgb("""+str(Colors.str_from_color(Colors.green200))+""");
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    color: rgb("""+str(Colors.str_from_color(Colors.green200))+""");
  }   

"""
