import sqlite3
import os, shutil, subprocess, pyperclip
from collections import namedtuple
from lib.config import *
from platform import system as get_os


class DBHandler:
    def __init__(self):
        dbExists = os.path.isfile('./bibliography.sqlite')
        self.db_connection = None
        try:
            self.db_connection = sqlite3.connect('./bibliography.sqlite')
        except splite3.Error as e:
            print(e)
        self.db_cursor = self.db_connection.cursor()
        if dbExists is False:
            self.create_table_structure()
        self.db_cursor.execute("PRAGMA table_info(documents)")
        self.docTable_numberOfColumns = len(self.db_cursor.fetchall())

    def create_table_structure(self):
        self.db_cursor.execute("CREATE TABLE IF NOT EXISTS documents ("+
                               "ID integer PRIMARY KEY AUTOINCREMENT UNIQUE,"+ 
                               "Type text, Token text, Authors text, Title text,"+
                               "Year integer, Journal text, Volume integer, Number integer,"+
                               "Pages text, Doi text, Publisher text, ISBN text,"+
                               "Series text, Address text, Thesis text, School text,"+
                               "Note text, Notes text);")
        self.db_cursor.execute("CREATE TABLE IF NOT EXISTS keywords ("+
                               "ID integer PRIMARY KEY AUTOINCREMENT UNIQUE,"+
                               "keyword text);")
        self.db_cursor.execute("CREATE TABLE IF NOT EXISTS documentKeywords ("+
                               "documentID integer, keywordID integer);")

    def get_table(self, table_name):
        self.db_cursor.execute("SELECT * FROM "+table_name)
        return self.db_cursor.fetchall()

    def table_select_where(self, table, select_list, where_dict):
        query = "SELECT "
        for item in select_list:
            query = query + item + ","
        query = query[:-1]
        query = query + " FROM " + table + " WHERE "
        for item in where_dict:
            query = query + str(item) + " = '" + str(where_dict[item]) + "' AND "
        query = query[:-5]
        self.db_cursor.execute(query)
        return self.db_cursor.fetchall()

    def delete_entry(self, table_name, data_dict):
        query = "DELETE FROM "+table_name+" WHERE "
        for item in data_dict:
            query = query + str(item) + " = '"+str(data_dict[item])+"' AND "
        query = query[:-5]
        self.db_cursor.execute(query)
        self.db_connection.commit()

    def add_data(self, table_name, dataset):
        columns = ""
        values = ""
        for item in dataset:
            dataset[item] = str(dataset[item]).replace("'", "''")
            columns = columns + item + ","
            values = values + "'" + dataset[item] + "',"
        columns = columns[:-1]
        values = values[:-1]
        self.db_cursor.execute("INSERT INTO "+table_name+"("+columns+") VALUES("+values+");")
        self.db_connection.commit()
        return self.db_cursor.lastrowid

    def modify_dataset(self, table, db_entry_id, doc_dict):
        values = ""
        for item in doc_dict:
            doc_dict[item] = str(doc_dict[item]).replace("'", "''")
            values = values + str(item) + " = '" + str(doc_dict[item]) + "', "
        values = values[:-2]
        self.db_cursor.execute("UPDATE "+table+" SET "+values+" WHERE ID = "+str(db_entry_id))
        self.db_connection.commit()

    def search_by_field(self, table_name, search_dict):
        query = DBStructure.doctype.title+" LIKE '%"+search_dict[DBStructure.doctype.title]+"%' AND "
        query = query + DBStructure.title.title+" LIKE '%"+search_dict[DBStructure.title.title]+"%' AND "
        query = query + DBStructure.author.title+" LIKE '%"+search_dict[DBStructure.author.title]+"%'"
        self.db_cursor.execute("SELECT * FROM "+table_name+" WHERE "+query)
        return self.db_cursor.fetchall()

    def check_for_value(self, table_name, field_name, search_value):
        self.db_cursor.execute("SELECT * FROM "+table_name+" WHERE "+field_name+" = '"+search_value+"'")
        if self.db_cursor.fetchall():
            return True
        else:
            return False

    def __del__(self):
        self.db_connection.close()


class DBStructure:
    db_col = namedtuple('db_col', 'title id block bibtex')
    # unique ID of entry
    ID = db_col(title='ID', id=0, block='none', bibtex='none')
    doctype = db_col(title='Type', id=1, block='none', bibtex='none')
    # common entries
    token = db_col(title='Token', id=2, block='article,book,thesis,other', bibtex='none')
    author = db_col(title='Authors', id=3, block='article,book,thesis,other', bibtex='    author    ')
    title = db_col(title='Title', id=4, block='article,book,thesis,other', bibtex='    title     ')
    year = db_col(title='Year', id=5, block='article,book,thesis,other', bibtex='    year      ')
    # article specific entries
    journal = db_col(title='Journal', id=6, block='article', bibtex='    journal   ')
    volume = db_col(title='Volume', id=7, block='article,book', bibtex='    volume    ')
    number = db_col(title='Number', id=8, block='article', bibtex='    number    ')
    pages = db_col(title='Pages', id=9, block='article', bibtex='    pages     ')
    doi = db_col(title='DOI', id=10, block='article,book', bibtex='    doi       ')
    # book specific entries
    publisher = db_col(title='Publisher', id=11, block='book', bibtex='    publisher ')
    isbn = db_col(title='ISBN', id=12, block='book', bibtex='    isbn      ')
    series = db_col(title='Series', id=13, block='book', bibtex='    series    ')
    address = db_col(title='Address', id=14, block='book', bibtex='    address   ')
    # thesis specific entries
    thesis_type = db_col(title='Thesis', id=15, block='thesis', bibtex='    type      ')
    school = db_col(title='School', id=16, block='thesis', bibtex='    school    ')
    # other specific entries
    note = db_col(title="Note", id=17, block='article,book,thesis,other', bibtex='    note      ')
    # note entry
    notes = db_col(title='Notes', id=18, block='none', bibtex='none')

    db_elements = [ID, doctype, token, author, title, year, journal, volume, number, pages, doi,
                   publisher, isbn, series, address, thesis_type, school, note, notes]

    def check_completeness(self, data_input):

        if DBHandler().check_for_value("documents", "token", data_input[self.token.id]):
            ErrorBox("Duplicate", "The given token already exists in the database")
            return False

        common_elements = [element for element in DBStructure.db_elements if element.title in "token,author,title,year,doctype"]
        for elem in common_elements:
            if data_input[elem.id] == "" or data_input[elem.id] is None:
                ErrorBox("Missing data", "Please insert a "+elem.title)
                return False

        if data_input[self.doctype.id] == "article":
            if data_input[self.journal.id] == "" or data_input[self.journal.id] is None:
                ErrorBox("Missing data", "Please insert a "+journal.title)
                return False
        elif data_input[self.doctype.id] == "book":
            if data_input[self.publisher.id] == "" or data_input[self.publisher.id] is None:
                ErrorBox("Missing data", "Please insert a "+publisher.title)
                return False
        elif data_input[self.doctype.id] == "thesis":
            if data_input[self.school.id] == "" or data_input[self.school.id] is None:
                ErrorBox("Missing data", "Please insert a "+school.title)
                return False
            elif data_input[self.thesis_type.id] == "" or data_input[self.thesis_type.id] is None:
                ErrorBox("Missing data", "Please insert a "+thesis_type.title)
                return False
        return True


class FileHandler:
    def move_file(self, origin, dataset):
        base_path = str(Configs.get_file_path())
        if not os.path.exists(base_path + "/" + dataset[DBStructure.doctype.title]):
            os.makedirs(base_path + "/" + dataset[DBStructure.doctype.title])
        shutil.move(origin, base_path + "/" + dataset[DBStructure.doctype.title] + "/" + dataset[DBStructure.token.title] + ".pdf")

    def relocate_file(self, old_path, new_path):
        try:
            shutil.move(old_path, new_path)
        except:
            print("ERROR: Could not find file at given location")

    def open_file(self, element_data=None):
        if element_data is None:
            filepath = str(Configs.get_file_path())
        else:
            filepath = str(Configs.get_file_path()) + "/" + str(element_data[DBStructure.doctype.id]) + "/" + str(element_data[DBStructure.token.id]) + ".pdf"
        current_os = str(get_os())
        if current_os == "Windows":
            os.startfile(filepath)
        elif current_os == "Linux":
            subprocess.run(['xdg-open', filepath])
        elif current_os == "Darwin":
            subprocess.run(['open', filepath])

    def get_link(self, element_data):
        full_path = os.path.realpath(__file__)
        config_path = str(Configs.get_file_path())
        if config_path is "./files":
            base_path = str(os.path.dirname(os.path.dirname(full_path))).replace("\\", "/")
            base_path = base_path + "/files"
        else:
            base_path = config_path
        link = base_path+"/"+element_data[DBStructure.doctype.id]+"/"+element_data[DBStructure.token.id]+".pdf"
        self.copy_to_clipboard(link)

    def copy_to_clipboard(self, text):
        pyperclip.copy(text)

    def copy_to_file(self, text):
        f = open("bibfile.bib", "w+")
        f.write(text)
        f.close()

    def delete_file(self, dataset):
        base_path = str(Configs.get_file_path())
        if os.path.exists(base_path+"/"+dataset[DBStructure.doctype.id]+"/"+dataset[DBStructure.token.id]+".pdf"):
            os.remove(base_path+"/"+dataset[DBStructure.doctype.id]+"/"+dataset[DBStructure.token.id]+".pdf")

