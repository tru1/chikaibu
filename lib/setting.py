from lib.helpers import *


class SettingsPage(QWidget):
    def __init__(self, parent):
        super(SettingsPage, self).__init__(parent)
        main_layout = QHBoxLayout(self)
        main_layout.setAlignment(Qt.AlignLeft)
        main_layout.addSpacing(20)
        # add color changing function
        color_widget = QWidget()
        color_widget.setFixedWidth(360)
        self.color_layout = QVBoxLayout(color_widget)
        self.color_layout.setSpacing(0)
        self.color_layout.setAlignment(Qt.AlignTop)
        color_title = CustomLabel(color_widget, Colors.text_high, 14, True, "Edit design colors")
        color_subtitle = CustomLabel(color_widget, Colors.text_disabled, 10, True, "(requires app restart)")
        self.color_layout.addSpacing(20)
        self.color_layout.addWidget(color_title, 0, Qt.AlignCenter)
        self.color_layout.addWidget(color_subtitle, 0, Qt.AlignCenter)
        self.color_layout.addSpacing(10)
        self.color_layout.addWidget(ColorView("Background", Colors.dark_gray, "dark_gray"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Gray-1", Colors.dp01, "gray01"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Gray-2", Colors.dp08, "gray08"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Accent-1", Colors.green50, "accent1"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Accent-2", Colors.green200, "accent3"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Accent-3", Colors.green400, "accent5"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Text-1", Colors.text_high, "text1"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Text-2", Colors.text_medium, "text2"), 0, Qt.AlignTop)
        self.color_layout.addWidget(ColorView("Text-3", Colors.text_disabled, "text3"), 0, Qt.AlignTop)
        buttons_widget = QWidget()
        buttons_layout = QHBoxLayout(buttons_widget)
        font = QApplication.font()
        font.setPointSize(12)
        font.setBold(True)
        save_button = QPushButton("save")
        save_button.setObjectName("bulk")
        save_button.setFont(font)
        save_button.clicked.connect(self.save_colors)
        buttons_layout.addWidget(save_button)
        reset_button = QPushButton("restore default")
        reset_button.setObjectName("bulk")
        reset_button.setFont(font)
        reset_button.clicked.connect(self.restore_default_colors)
        buttons_layout.addWidget(reset_button)
        self.color_layout.addWidget(buttons_widget)
        main_layout.addWidget(color_widget, 0, Qt.AlignLeft)
        # editable filepath
        main_layout.addSpacing(80)
        right_widget = QWidget()
        right_layout = QVBoxLayout(right_widget)
        right_layout.setAlignment(Qt.AlignTop)
        main_layout.addWidget(right_widget)
        file_widget = QWidget()
        file_widget.setFixedWidth(450)
        file_layout = QVBoxLayout(file_widget)
        file_layout.setAlignment(Qt.AlignTop)
        file_layout.setSpacing(0)
        path_widget = QWidget()
        path_layout = QHBoxLayout(path_widget)
        file_title = CustomLabel(color_widget, Colors.text_high, 14, True, "Change file storage location")
        file_button = QPushButton("browse folders")
        file_button.setObjectName("bulk")
        file_button.setFixedSize(140, 30)
        file_button.clicked.connect(self.get_file_path)
        file_button.setFont(font)
        self.path_edit = QLineEdit()
        font = QApplication.font()
        font.setPointSize(10)
        self.path_edit.setFont(font)
        self.path_edit.setFixedWidth(250)
        self.path_edit.setText(str(Configs.get_file_path()))
        font.setPointSize(12)
        font.setBold(True)
        path_layout.addWidget(self.path_edit)
        path_layout.addSpacing(15)
        path_layout.addWidget(file_button)
        file_layout.addWidget(file_title, 0, Qt.AlignCenter)
        file_layout.addSpacing(15)
        file_layout.addWidget(path_widget, 0, Qt.AlignCenter)
        save_button = QPushButton("migrate files")
        save_button.setObjectName("bulk")
        save_button.setFixedSize(150, 30)
        save_button.setFont(font)
        save_button.clicked.connect(self.move_file_base)
        file_layout.addWidget(save_button, 0, Qt.AlignCenter)
        right_layout.addSpacing(10)
        right_layout.addWidget(file_widget)
        right_layout.addSpacing(20)
        right_layout.addWidget(HLine(Colors.dp08, 2, 2))
        right_layout.addSpacing(20)
        # table for keywords with count
        right_layout.addWidget(CustomLabel(file_widget, Colors.text_high, 14, True, "Keyword counter"), 0, Qt.AlignCenter)
        right_layout.addSpacing(5)
        self.keyword_table_widget = QWidget()
        pal = QPalette()
        pal.setColor(self.keyword_table_widget.backgroundRole(), Colors.dp01)
        self.keyword_table_widget.setPalette(pal)
        # Scroll Area Properties
        scroll = QScrollArea(self)
        scroll.setFrameStyle(QFrame.NoFrame)
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(self.keyword_table_widget)
        scroll.setFixedHeight(330)
        self.set_keyword_table()
        right_layout.addWidget(scroll, 0, Qt.AlignCenter)

    def set_keyword_table(self):
        # manage layout
        QWidget().setLayout(self.keyword_table_widget.layout())
        keyword_table_layout = QVBoxLayout(self.keyword_table_widget)
        keyword_table_layout.setSpacing(0)
        keyword_table_layout.setAlignment(Qt.AlignTop)
        keyword_table_layout.setContentsMargins(0,0,0,0)
        # get keyword occurence numbers
        keyword_table = DBHandler().get_table("keywords")
        occurence_dict = {}
        for item in keyword_table:
            occurence = len(DBHandler().table_select_where("documentKeywords", ["*"], {"keywordID": item[0]}))
            occurence_dict[item[0]] = occurence
        for keyID in sorted(occurence_dict, key=occurence_dict.get, reverse=True):
            keyWord = DBHandler().table_select_where("keywords", ["keyword"], {"ID": keyID})
            keyword_table_layout.addWidget(KeywordEntry(keyWord[0][0], keyID, occurence_dict[keyID]), 0, Qt.AlignCenter)
      
    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        painter.drawRoundedRect(8, 10, self.width()-16, self.height()-20, 10.0, 10.0)

    def restore_default_colors(self):
        old_path = Configs.get_file_path()
        try:
            os.remove("config.ini")
        except:
            print("ERROR: config.ini not found")
        Configs.create()
        Configs.set('files', 'path', str(old_path))
        for i in range(1, self.color_layout.count()):
            data_elem = self.color_layout.itemAt(i).widget()
            if isinstance(data_elem, ColorView):
                data_elem.reload()

    def save_colors(self):
        for i in range(1, self.color_layout.count()):
            data_elem = self.color_layout.itemAt(i).widget()
            if isinstance(data_elem, ColorView):
                data_elem.store_current()

    def get_file_path(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            self.path_edit.setText(str(filenames[0]))

    def move_file_base(self):
        box = QMessageBox()
        box.setIcon(QMessageBox.Question)
        box.setWindowTitle("Move Files")
        box.setText("Are you sure to move all files in database to a new location?")
        box.addButton(QMessageBox.Yes)
        box.addButton(QMessageBox.Cancel)
        if box.exec_() == QMessageBox.Yes:
            path = self.path_edit.text()
            if path != "" or None:
                if not os.path.exists(str(path)):
                    os.makedirs(str(path))
                old_path = Configs.get_file_path()
                try:
                    FileHandler().relocate_file(str(old_path)+"/article", str(path))
                except:
                    print("ERROR: No article folder found")
                try:
                    FileHandler().relocate_file(str(old_path)+"/book", str(path))
                except:
                    print("ERROR: No book folder found")
                try:
                    FileHandler().relocate_file(str(old_path)+"/thesis", str(path))
                except:
                    print("ERROR: No thesis folder found")
                try:
                    FileHandler().relocate_file(str(old_path)+"/other", str(path))
                except:
                    print("ERROR: No other folder found")
                Configs.set('files', 'path', str(path))


class ColorView(QWidget):
    def __init__(self, name, color, field_name):
        super(ColorView, self).__init__()
        self.color = color
        self.field_name = field_name
        font = QApplication.font()
        font.setPointSize(12)
        layout = QHBoxLayout(self)
        layout.setAlignment(Qt.AlignLeft)
        layout.setSpacing(5)
        layout.addWidget(CustomLabel(self, Colors.text_high, 12, False, str(name), 150))
        self.red = QLineEdit(self)
        self.red.setText(str(self.color.red()))
        self.red.setFixedWidth(50)
        self.red.setFont(font)
        self.red.textChanged.connect(self.change_color)
        layout.addWidget(self.red)
        self.green = QLineEdit(self)
        self.green.setText(str(self.color.green()))
        self.green.setFixedWidth(50)
        self.green.setFont(font)
        self.green.textChanged.connect(self.change_color)
        layout.addWidget(self.green)
        self.blue = QLineEdit(self)
        self.blue.setText(str(self.color.blue()))
        self.blue.setFixedWidth(50)
        self.blue.setFont(font)
        self.blue.textChanged.connect(self.change_color)
        layout.addWidget(self.blue)
        self.line = HLine(Colors.green200, 50, 50)
        self.line.setFixedWidth(30)
        self.line.setFixedHeight(30)
        self.line.setStyleSheet("border:none; background:rgb("+Colors.str_from_color(self.color)+");")
        layout.addWidget(self.line)

    def change_color(self):
        r = self.red.text()
        g = self.green.text()
        b = self.blue.text()
        self.line.setStyleSheet("border:none; background:rgb("+self.valid(r)+","+self.valid(g)+","+self.valid(b)+");")

    def valid(self, value):
        try: 
            if int(value) < 0:
                value = 0
            if int(value) > 255:
                value = 255
            return str(value)
        except:
            return str(0)

    def reload(self):
        r = str(Configs.get_color(self.field_name)[0])
        g = str(Configs.get_color(self.field_name)[1])
        b = str(Configs.get_color(self.field_name)[2])
        self.red.setText(r)
        self.green.setText(g)
        self.blue.setText(b)
        self.line.setStyleSheet("border:none; background:rgb("+r+","+g+","+b+");")

    def store_current(self):
        Configs.set(self.field_name, "red", self.red.text())
        Configs.set(self.field_name, "green", self.green.text())
        Configs.set(self.field_name, "blue", self.blue.text())


class KeywordEntry(QWidget):
    def __init__(self, key, keyID, occurence):
        super(KeywordEntry, self).__init__()
        self.keyID = keyID
        entry_layout = QHBoxLayout(self)
        font = QApplication.font()
        font.setPointSize(10)
        metrics = QFontMetrics(font)
        key_elided = metrics.elidedText(str(key), Qt.ElideRight, 290)
        self.key_label = CustomLabel(self, Colors.text_medium, 10, False, key_elided, 300)
        edit_button = QPushButton(self)
        edit_button.clicked.connect(lambda: self.edit_keyword(edit_button, key))
        button_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("edit.png")), Colors.text_medium)
        edit_button.setIcon(QIcon(button_pix))
        entry_layout.addWidget(edit_button)
        entry_layout.addWidget(self.key_label)
        entry_layout.addWidget(CustomLabel(self, Colors.text_high, 10, True, str(occurence), 50))

    def edit_keyword(self, button, key):
        key_edit = QLineEdit()
        key_edit.setFixedWidth(300)
        font = QApplication.font()
        font.setPointSize(10)
        key_edit.setFont(font)
        key_edit.setText(self.key_label.text())
        self.layout().replaceWidget(self.key_label, key_edit)
        self.key_label.setParent(None)
        self.key_label = key_edit
        button_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("save.png")), Colors.text_medium)
        button.setIcon(QIcon(button_pix))
        button.clicked.connect(lambda: self.save_keyword(button, self.key_label.text()))

    def save_keyword(self, button, key):
        button.clicked.connect(lambda: self.edit_keyword(button, self.key_label.text()))
        font = QApplication.font()
        font.setPointSize(10)
        metrics = QFontMetrics(font)
        key_elided = metrics.elidedText(str(self.key_label.text()), Qt.ElideRight, 290)
        new_key_label = CustomLabel(self, Colors.text_medium, 10, False, key_elided, 300)
        self.layout().replaceWidget(self.key_label, new_key_label)
        self.key_label.setParent(None)
        self.key_label = new_key_label
        button_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("edit.png")), Colors.text_medium)
        button.setIcon(QIcon(button_pix))
        DBHandler().modify_dataset("keywords", self.keyID, {"keyword": str(self.key_label.text())})

