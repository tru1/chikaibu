from lib.helpers import *
from lib.metaData import metaHandler


class AddElement(QWidget):

    def __init__(self, parent):
        super(AddElement, self).__init__(parent)
        add_elem_layout = QHBoxLayout(self)
        add_elem_layout.setContentsMargins(15, 8, 15, 8)
        self.add_data_fields_widget = AddData(self)

        # left half of page
        left_widget = QWidget()
        left_layout = QVBoxLayout(left_widget)
        left_layout.setContentsMargins(0, 0, 0, 0)
        left_layout.setAlignment(Qt.AlignTop)
        # create selector widget
        selector_widget = QWidget()
        selector_layout = QHBoxLayout(selector_widget)
        selector_layout.setContentsMargins(0, 0, 0, 0)
        selector_label = CustomLabel(self, Colors.text_high, 14, True, "Type ")
        selector_layout.addWidget(selector_label)
        # create selector switch button
        type_switch_w = QWidget()
        type_switch_l = QHBoxLayout(type_switch_w)
        type_switch_l.setContentsMargins(0, 0, 0, 0)
        type_switch_l.setSpacing(0)
        type_switch_l.setAlignment(Qt.AlignHCenter)
        self.radio1 = QRadioButton(" Article")
        radio2 = QRadioButton("  Book")
        radio3 = QRadioButton(" Thesis")
        radio4 = QRadioButton(" Other")
        self.radio1.setFixedWidth(75)
        radio2.setFixedWidth(75)
        radio3.setFixedWidth(75)
        radio4.setFixedWidth(75)
        self.radio1.setObjectName("left_button")
        self.radio1.setChecked(True)
        radio2.setObjectName("mid_button")
        radio3.setObjectName("mid_button")
        radio4.setObjectName("right_button")
        new_font = QApplication.font()
        new_font.setPointSize(12)
        new_font.setBold(True)
        self.radio1.setFont(new_font)
        radio2.setFont(new_font)
        radio3.setFont(new_font)
        radio4.setFont(new_font)
        self.radio1.toggled.connect(lambda: self.add_data_fields_widget.check_signal("article", self.radio1.isChecked()))
        radio2.toggled.connect(lambda: self.add_data_fields_widget.check_signal("book", radio2.isChecked()))
        radio3.toggled.connect(lambda: self.add_data_fields_widget.check_signal("thesis", radio3.isChecked()))
        radio4.toggled.connect(lambda: self.add_data_fields_widget.check_signal("other", radio4.isChecked()))
        type_switch_l.addWidget(self.radio1)
        type_switch_l.addWidget(radio2)
        type_switch_l.addWidget(radio3)
        type_switch_l.addWidget(radio4)
        selector_layout.addWidget(type_switch_w)
        left_layout.addSpacing(25)
        left_layout.addWidget(selector_widget, 0, Qt.AlignCenter)
        left_layout.addSpacing(0)
        left_layout.addWidget(self.add_data_fields_widget)
        add_elem_layout.addWidget(left_widget)
        add_elem_layout.addSpacing(15)
        # create right side (note view)
        note_widget = QWidget()
        note_layout = QVBoxLayout(note_widget)
        note_label = CustomLabel(note_widget, Colors.text_medium, 16, True, "Notes")
        note_layout.addWidget(note_label, 0, Qt.AlignCenter)
        self.note_field = NoteView(self, True)
        note_layout.addWidget(self.note_field)
        add_elem_layout.addWidget(note_widget)

    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        painter.drawRoundedRect(8, 10, self.width()-16, self.height()-20, 10.0, 10.0)

    def save_data(self, file_path):
        dataset, keywords = self.add_data_fields_widget.collect_data()
        dataset[DBStructure.notes.title] = self.note_field.get_text()
        # check for .pdf file
        if (file_path is None) or file_path == "":
            ErrorBox("File missing", "No .pdf has been chosen or the file is missing")
            return False
        # save data to sqlite table and move .pdf file if all necessary data are filled in
        if Helpers().check_completeness(dataset):   
            FileHandler().move_file(file_path, dataset)
            new_doc_id = DBHandler().add_data("documents", dataset)
            for key_id in keywords:
                junction_data = {"documentID": str(new_doc_id), "keywordID": str(key_id)}
                DBHandler().add_data("documentKeywords", junction_data)
            return True
        else:
            return False

    def refresh(self):
        self.add_data_fields_widget.set_element_type("article")
        self.radio1.setChecked(True)
        self.note_field.refresh()


class AddData(QScrollArea):
    def __init__(self, parent):
        super(AddData, self).__init__(parent)
        self.setFixedWidth(600)
        self.setFrameStyle(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)
        # create main widget in scroll area
        self.scroll_widget = QWidget()
        pal = QPalette()
        pal.setColor(self.scroll_widget.backgroundRole(), Colors.dp01)
        self.scroll_widget.setPalette(pal)
        self.setWidget(self.scroll_widget)
        # set content
        self.set_element_type("article")
        self.current_page = "article"

    def check_signal(self, selected_type, signal):
        if signal is True:
            self.set_element_type(selected_type)
            self.current_page = selected_type

    def set_downloaded_data(self, selected_type, doi):
        dat = None
        if selected_type == "article":
            if "arXiv:" in doi:
                dat, success = metaHandler.get_arxiv_article_meta(doi)
            else:
                dat, success = metaHandler.get_article_meta(doi)
        if selected_type == "book":
            dat, success = metaHandler.get_book_meta(doi)
        if success is False:
            ErrorBox("DOI not found", "The DOI you entered was not found in the CrossRef or arXiv database")
        for i in range(1, self.scroll_widget.layout().count()):
            data_element  = self.scroll_widget.layout().itemAt(i).widget()
            if isinstance(data_element, SingleDataElement) is True:
                data_element.set_text(str(dat[data_element.get_name()]))

    def set_element_type(self, selected_type):
        QWidget().setLayout(self.scroll_widget.layout())
        new_layout = QVBoxLayout(self.scroll_widget)
        new_layout.setContentsMargins(20, 20, 20, 20)
        new_layout.setAlignment(Qt.AlignTop)
        new_layout.addSpacing(20)

        # create controls to downlad meta-data
        if selected_type in "article,book":
            download_widget = QWidget()
            download_layout = QHBoxLayout(download_widget)
            download_layout.setContentsMargins(0, 0, 0, 0)
            download_layout.setAlignment(Qt.AlignCenter)
            # download field
            download_field = QLineEdit()
            new_font10 = QApplication.font()
            new_font10.setPointSize(10)
            download_field.setFont(new_font10)
            download_field.setFixedWidth(210)
            download_field.setPlaceholderText("DOI")
            download_layout.addWidget(download_field)
            # create download button
            download_button = QPushButton("Download meta-data")
            new_font12 = QApplication.font()
            new_font12.setPointSize(12)
            new_font12.setBold(True)
            download_button.setFont(new_font12)
            download_button.clicked.connect(lambda: self.set_downloaded_data(selected_type, download_field.text()))
            download_button.setFixedSize(210, 30)
            download_layout.addWidget(download_button)
            # create tooltip
            faq_label = QLabel()
            faq_label.setFont(new_font12)
            faq_label.setText("?")
            faq_label.setAlignment(Qt.AlignCenter)
            faq_label.setFixedWidth(30)
            faq_label.setToolTip("<html><head/><body><p>Enter a DOI of shape '10.1081/E-ELIS3-120044418' "\
                                 "or an arXiv ID of shape 'arXiv:1108.2700', "\
                                 "it will be looked up online and the found data are downloaded</p></body></html>")
            download_layout.addWidget(faq_label)
            new_layout.addWidget(download_widget)
            new_layout.addSpacing(40)
        # add data fields
        elements = [item for item in DBStructure.db_elements if selected_type in item.block]
        for x in elements:
            new_layout.addWidget(SingleDataElement(self, x.title, "", True, x.id))
            new_layout.addSpacing(10)
        # section for keywords
        new_layout.addSpacing(30)
        self.keyword_cloud = KeywordCloud(self)
        new_layout.addWidget(KeywordAdd(self.keyword_cloud))
        new_layout.addWidget(KeywordSelector(self.keyword_cloud))
        new_layout.addWidget(self.keyword_cloud)

    def collect_data(self):
        data_layout = self.scroll_widget.layout()
        dataset = {}
        keywords = None
        for i in range(1, data_layout.count()):
            data_element  = data_layout.itemAt(i).widget()
            if isinstance(data_element, SingleDataElement) is True:
                element_key =  DBStructure.db_elements[data_element.get_id()].title
                dataset[element_key] = data_layout.itemAt(i).widget().get_text()
            if isinstance(data_element, KeywordCloud) is True:
                keywords = list(data_element.get_current_keywords().keys())
        dataset[DBStructure.doctype.title] = self.current_page
        return dataset, keywords

