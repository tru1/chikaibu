from lib.helpers import *


class Library(QWidget):
    query_list = {DBStructure.title.title: "", DBStructure.author.title: "", DBStructure.doctype.title: ""}
    query_data = None

    def __init__(self, parent):
        super(Library, self).__init__(parent)
        widget_layout = QHBoxLayout(self)
        self.options_frame = OptionsFrame(self)
        widget_layout.addWidget(self.options_frame)
        widget_layout.addSpacing(10)
        self.list_frame = ListFrame(self, parent)
        widget_layout.addWidget(self.list_frame)
        self.query_data = DBHandler().get_table("documents")

    def change_list_filter(self, filter_id, query_info):
        self.query_list[filter_id] = query_info
        self.query_data = DBHandler().search_by_field("documents", self.query_list)
        self.list_frame.create_element_list(self.query_data, self.options_frame.get_doc_set_from_keywords())

    def change_list_search(self, field, name):
        if field is True:
          field_name = DBStructure.title.title
          self.query_list[DBStructure.author.title] = ""
        else:
          field_name = DBStructure.author.title
          self.query_list[DBStructure.title.title] = ""
        self.query_list[field_name] = name
        self.query_data = DBHandler().search_by_field("documents", self.query_list)
        self.list_frame.create_element_list(self.query_data, self.options_frame.get_doc_set_from_keywords())

    def change_list_keywords(self):
        self.query_data = DBHandler().search_by_field("documents", self.query_list)
        self.list_frame.create_element_list(self.query_data, self.options_frame.get_doc_set_from_keywords())

    def get_current_query(self):
        return self.query_data

    def set_current_query(self, new_list):
        self.query_data = new_list

    def reset_list_and_search(self):
        self.options_frame.reset()
        db_data = DBHandler().get_table("documents")
        self.list_frame.create_element_list(db_data)


class OptionsFrame(QWidget):
    def __init__(self, parent):
        super(OptionsFrame, self).__init__(parent)
        self.setFixedWidth(270)
        options_layout = QVBoxLayout(self)
        options_layout.setAlignment(Qt.AlignTop)
        options_layout.setContentsMargins(8, 15, 8, 15)

        ### Search-Block
        search_in = QWidget()
        search_layout = QHBoxLayout(search_in)
        search_title = CustomLabel(self, Colors.text_high, 14, True, "Search in")
        search_layout.addWidget(search_title)
        # create switch between author and title
        search_switch_w = QWidget()
        search_switch_l = QHBoxLayout(search_switch_w)
        search_switch_l.setContentsMargins(0, 0, 0, 0)
        search_switch_l.setSpacing(0)
        search_switch_l.setAlignment(Qt.AlignHCenter)
        self.radio1 = QRadioButton("  Title")
        self.radio2 = QRadioButton("Author")
        self.radio1.setFixedWidth(68)
        self.radio2.setFixedWidth(68)
        self.radio1.setObjectName("left_button")
        self.radio2.setChecked(True)
        self.radio2.setObjectName("right_button")
        new_font = QApplication.font()
        new_font.setPointSize(11)
        new_font.setBold(True)
        self.radio1.setFont(new_font)
        self.radio2.setFont(new_font)
        search_switch_l.addWidget(self.radio1)
        search_switch_l.addWidget(self.radio2)
        search_layout.addWidget(search_switch_w)
        options_layout.addWidget(search_in, 0, Qt.AlignCenter)
        # search field
        self.search_field = QLineEdit()
        font10 = QApplication.font()
        font10.setPointSize(10)
        self.search_field.setFont(font10)
        self.search_field.setFixedWidth(search_in.minimumSizeHint().width())
        self.search_field.returnPressed.connect(lambda: parent.change_list_search(self.radio1.isChecked(), self.search_field.text()))
        options_layout.addWidget(self.search_field, 0, Qt.AlignCenter)
        # send to querry
        loupe_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("loupe.png")), Colors.text_high)
        search_button = QPushButton(" Search")
        search_button.setObjectName("bulk")
        new_font.setPointSize(12) 
        search_button.setFont(new_font)
        search_button.setIcon(QIcon(loupe_pix))
        search_button.clicked.connect(lambda: parent.change_list_search(self.radio1.isChecked(), self.search_field.text()))
        search_button.setFixedSize(110, 30)

        reset_button = QPushButton("Reset")
        reset_button.setObjectName("bulk")
        reset_button.setFont(new_font)
        reset_button.clicked.connect(lambda: parent.reset_list_and_search())
        reset_button.setFixedSize(110, 30)

        buttons_w = QWidget()
        buttons_l = QHBoxLayout(buttons_w)
        buttons_l.addWidget(search_button, 0 ,Qt.AlignCenter)
        buttons_l.addSpacing(10)
        buttons_l.addWidget(reset_button, 0 ,Qt.AlignCenter)

        options_layout.addSpacing(5)
        options_layout.addWidget(buttons_w, 0 ,Qt.AlignCenter)
        options_layout.addSpacing(30)
        options_layout.addWidget(HLine(Colors.dp08, 2, 2))
        options_layout.addSpacing(30)

        ### Doctype-Block
        doctype_widget = QWidget()
        doctype_layout = QHBoxLayout(doctype_widget)
        doctype_title = CustomLabel(self, Colors.text_high, 14, True, "Show only ")
        doctype_layout.addWidget(doctype_title)
        self.doctype_box = QComboBox(self)
        self.doctype_box.setFixedWidth(90)
        new_font.setPointSize(10)
        self.doctype_box.setFont(new_font)
        self.doctype_box.addItem("  - all - ", "")
        self.doctype_box.addItem("  article", "article")
        self.doctype_box.addItem("  book", "book")
        self.doctype_box.addItem("  thesis", "thesis")
        self.doctype_box.addItem("  other", "other")
        self.doctype_box.currentIndexChanged.connect(lambda: parent.change_list_filter(DBStructure.doctype.title, self.doctype_box.itemData(self.doctype_box.currentIndex())))
        doctype_layout.addWidget(self.doctype_box)
        options_layout.addWidget(doctype_widget, 0, Qt.AlignCenter)
        options_layout.addSpacing(30)
        options_layout.addWidget(HLine(Colors.dp08, 2, 2))
        options_layout.addSpacing(30)

        # Filter element by keywords block
        filter_title = CustomLabel(self, Colors.text_high, 14, True, "Filter by keywords")
        options_layout.addWidget(filter_title, 0, Qt.AlignCenter)
        self.cloud = KeywordCloud(self)
        self.cloud.keywordChanged.connect(lambda: parent.change_list_keywords())
        self.key_selector = KeywordSelector(self.cloud)
        options_layout.addWidget(self.key_selector)
        options_layout.addWidget(self.cloud)

    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        painter.drawRoundedRect(0, 0, self.width(), self.height(), 10.0, 10.0)

    def get_doc_set_from_keywords(self):
        key_list = list(self.cloud.get_current_keywords())
        doc_ids = [item[0] for item in DBHandler().get_table("documents")]
        for key in key_list:
            new_doc_ids = [item[0] for item in DBHandler().table_select_where("documentKeywords", ["documentID"], {"keywordID": str(key)} )]
            doc_ids = list(set(doc_ids).intersection(new_doc_ids))
        return doc_ids

    def reset(self):
        self.radio2.setChecked(True)
        self.search_field.setText("")
        self.doctype_box.setCurrentIndex(0)
        self.key_selector.setCurrentIndex(0)
        self.cloud.purge_cloud()
        self.key_selector.refresh()


class ListFrame(QWidget):
    def __init__(self, parent, content_frame):
        super(ListFrame, self).__init__(parent)
        self.parent = parent
        db_data = DBHandler().get_table("documents")
        self.content = content_frame
        self.scrollWidget = QWidget()
        self.scrollWidget.setLayout(QVBoxLayout())
        self.create_element_list(db_data)

        # Scroll Area Properties
        scroll = QScrollArea(self)
        scroll.setStyleSheet("background: transparent;")
        scroll.setFrameStyle(QFrame.NoFrame)
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setWidget(self.scrollWidget)

        # Scroll Area Layer add
        v_layout = QVBoxLayout(self)
        v_layout.setContentsMargins(0, 0, 0, 0)
        v_layout.addWidget(scroll)

    def create_element_list(self, query_elements, doc_ids=None):
        if doc_ids is not None:
            query_elements = [doc_element for doc_element in query_elements if doc_element[DBStructure.ID.id] in doc_ids]
        self.parent.set_current_query(query_elements)
        new_list = QVBoxLayout()
        new_list.setContentsMargins(0, 0, 0, 0)
        new_list.setAlignment(Qt.AlignTop)
        for i in range(len(query_elements)-1, -1, -1):
            new_list.addWidget(ListElement(self, query_elements[i], self.content))
            new_list.addSpacing(3)
        QWidget().setLayout(self.scrollWidget.layout())
        self.scrollWidget.setLayout(new_list)


class ListElement(QWidget):
    def __init__(self, parent, data, content_frame):
        super(ListElement, self).__init__(parent)
        self._parent = parent
        element_layout = QHBoxLayout(self)
        element_layout.setContentsMargins(15, 5, 15, 5)
        pic = QLabel()
        pic.setFixedWidth(50)
        pic.setAlignment(Qt.AlignCenter)
        if data[DBStructure.doctype.id] == "article":
            article_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("article.png")), Colors.green200)
            pic.setPixmap(article_pix)
        if data[DBStructure.doctype.id] == "book":
            book_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("book.png")), Colors.green200)
            pic.setPixmap(book_pix)
        if data[DBStructure.doctype.id] == "thesis":
            thesis_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("thesis.png")), Colors.green200)
            pic.setPixmap(thesis_pix)
        if data[DBStructure.doctype.id] == "other":
            other_pix = Helpers.changeImageColor(QImage(Helpers.resource_path("other.png")), Colors.green200)
            pic.setPixmap(other_pix)
        self.element_text = QWidget()
        self.element_text.setFixedHeight(self.height()+8)
        text_layout = QVBoxLayout(self.element_text)
        text_layout.setContentsMargins(0, 0, 0, 0)
        shift = self.layout().getContentsMargins()[0] + self.layout().getContentsMargins()[2]
        title = TitleLabel(self.element_text, data[DBStructure.title.id], data[DBStructure.ID.id], shift)
        author = AuthorLabel(self.element_text, data, shift)
        text_layout.addWidget(title)
        text_layout.setSpacing(0)
        text_layout.addWidget(author)
        element_layout.addWidget(pic)
        element_layout.addWidget(self.element_text) 
        self.mousePressEvent = partial(content_frame.switch, page_id["view_element"], data[DBStructure.ID.id])

    def paintEvent(self, ev):
        self.resize(self._parent.width(), self.height())
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        shift = self.layout().getContentsMargins()[0] + self.layout().getContentsMargins()[2]
        painter.drawRoundedRect(0, 0, self._parent.width()-shift, self.height(), 10.0, 10.0)


class TitleLabel(QLabel):
    def __init__(self, parent, text, element_id, shift):
        super(TitleLabel, self).__init__(parent)
        self._shift = shift
        self.setStyleSheet(f"color: rgb({Colors.text_high.red()},{Colors.text_high.green()},{Colors.text_high.blue()});")
        self._parent = parent
        font12 = QApplication.font()
        font12.setPointSize(12)
        self.setFont(font12)
        self.setText(str(text))

    def paintEvent(self, ev):
        painter = QPainter(self)
        metrics = QFontMetrics(self.font())
        shift = self._parent.layout().getContentsMargins()[0] + self._parent.layout().getContentsMargins()[2]
        elided = metrics.elidedText(str(self.text()), Qt.ElideRight, self._parent.width()-self._shift)
        self.resize(self._parent.width(), self.height())
        painter.drawText(self.rect(), self.alignment(), elided)


class AuthorLabel(QLabel):
    def __init__(self, parent, element_data, shift):
        super(AuthorLabel, self).__init__(parent)
        self._shift = shift
        self.setStyleSheet(f"color: rgb({Colors.text_disabled.red()},{Colors.text_disabled.green()},{Colors.text_disabled.blue()});")
        self._parent = parent
        font10 = QApplication.font()
        font10.setPointSize(10)
        self.setFont(font10)
        if element_data[DBStructure.doctype.id] == "thesis":
            self.setText(str(element_data[DBStructure.thesis_type.id]+" - "+element_data[DBStructure.author.id]))
        else:
            self.setText(str(element_data[DBStructure.author.id]))

    def paintEvent(self, ev):
        painter = QPainter(self)
        metrics = QFontMetrics(self.font())
        elided = metrics.elidedText(str(self.text()), Qt.ElideRight, self._parent.width()-self._shift)
        self.resize(self._parent.width(), self.height())
        painter.drawText(self.rect(), self.alignment(), elided)
