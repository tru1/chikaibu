from lib.helpers import *


class EditElement(QWidget):
    element_data = None

    def __init__(self, parent):
        super(EditElement, self).__init__(parent)

    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        painter.drawRoundedRect(8, 10, self.width()-16, self.height()-20, 10.0, 10.0)

    def get_id(self):
        return self.element_data[DBStructure.ID.id]

    def set_view_id(self, element_id):
        self.element_data = DBHandler().table_select_where("documents", ["*"], {"ID": element_id} )[0]
        QWidget().setLayout(self.layout())
        new_layout = QHBoxLayout(self)
        new_layout.setContentsMargins(15, 8, 15, 8)
        self.edit_data = EditData(self, self.element_data)
        new_layout.addWidget(self.edit_data)
        new_layout.addSpacing(15)
        note_widget = QWidget()
        note_layout = QVBoxLayout(note_widget)
        note_layout.addSpacing(13)

        title_widget = QWidget()
        title_layout = QHBoxLayout(title_widget)
        title_label = CustomLabel(self, Colors.text_medium, 16, True, "Notes")
        title_label.setToolTip("<html><head/><body><p>Enter a DOI of shape '10.1081/E-ELIS3-120044418' ")
        faq_label = QLabel()
        current_font = QFont(QApplication.font())
        current_font.setBold(True)
        current_font.setPointSize(14)
        faq_label.setFont(current_font)
        faq_label.setText("?")
        faq_label.setToolTip("<html><head/><body><p>The view of this notes interprets Markdown, a simple markup language,"\
                             "which allows to create e.g. tables and list in plain text format.</p>"\
                            "<p>So text can be bold by '**bold text**' or a task list can be created by '- [ ]', "\
                            "cheat sheets can be found online.</p></body></html>")
        title_layout.addWidget(title_label, 1, Qt.AlignCenter)
        title_layout.addWidget(faq_label, 0, Qt.AlignRight)

        note_layout.addWidget(title_widget)
        self.note_field = NoteView(self, True, self.element_data[DBStructure.notes.id])
        note_layout.addWidget(self.note_field)
        new_layout.addWidget(note_widget)

    def save_data(self):
        data, old_keywords, new_keywords, old_token = self.edit_data.collect_data()
        data[DBStructure.notes.title] = self.note_field.get_text()
        keywords_plus = set(new_keywords) - set(old_keywords)
        keywords_minus = set(old_keywords) - set(new_keywords)
        # update keyword junction table
        for key_id in keywords_plus:
            junction_data = {"documentID": str(data[DBStructure.ID.title]), "keywordID": str(key_id)}
            DBHandler().add_data("documentKeywords", junction_data)
        for key_id in keywords_minus:
            junction_data = {"documentID": str(data[DBStructure.ID.title]), "keywordID": str(key_id)}
            DBHandler().delete_entry("documentKeywords", junction_data)
        # save edited data to documents table
        if Helpers().check_completeness(data, old_token):
            DBHandler().modify_dataset("documents", data[DBStructure.ID.title], data)
            # relocate file (if token changed)
            if not old_token == data[DBStructure.token.title]:
                old_path = "./files/" + data[DBStructure.doctype.title] + "/" + old_token + ".pdf"
                new_path = "./files/" + data[DBStructure.doctype.title] + "/" + data[DBStructure.token.title] + ".pdf"
                FileHandler().relocate_file(old_path, new_path)
            return True
        else:
            return False


class EditData(QScrollArea):
    def __init__(self, parent, element_data):
        super(EditData, self).__init__(parent)
        self.setFixedWidth(600)
        self.setFrameStyle(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)
        self.type = element_data[DBStructure.doctype.id]
        self.doc_id = element_data[DBStructure.ID.id]
        self.token = element_data[DBStructure.token.id]

        # create main widget in scroll area
        self.scroll_widget = QWidget(self)
        pal = QPalette()
        pal.setColor(self.scroll_widget.backgroundRole(), Colors.dp01)
        self.scroll_widget.setPalette(pal)
        self.setWidget(self.scroll_widget)
        info_layout = QVBoxLayout(self.scroll_widget)
        info_layout.setContentsMargins(20, 20, 20, 20)
        info_layout.setAlignment(Qt.AlignTop)
        info_layout.setSpacing(20)

        # create title
        title_widget = QWidget()
        title_layout = QHBoxLayout(title_widget)
        pic = QLabel()
        pic.setFixedWidth(40)
        title_label = QLabel()
        if element_data[DBStructure.doctype.id] == "article":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("article.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Edit Article")
        elif element_data[DBStructure.doctype.id] == "book":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("book.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Edit Book")
        elif element_data[DBStructure.doctype.id] == "thesis":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("thesis.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Edit Thesis")
        elif element_data[DBStructure.doctype.id] == "other":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("other.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Edit Other")
        title_layout.addWidget(pic)
        title_layout.addWidget(title_label)
        info_layout.addWidget(title_widget, 0, Qt.AlignCenter)

        # create editable content
        elements = [item for item in DBStructure.db_elements if element_data[DBStructure.doctype.id] in item.block]
        for x in elements:
            info_layout.addWidget(SingleDataElement(self, x.title, str(element_data[x.id]), True, x.id))
        info_layout.addSpacing(10)
        info_layout.addWidget(HLine(Colors.dp08, 2, 2))
        info_layout.addSpacing(10)
        # add keyword view
        query_key_ids = DBHandler().table_select_where("documentKeywords", ["keywordID"], {"documentID": str(element_data[DBStructure.ID.id])} )
        cloud = KeywordCloud(parent)
        for key_id in query_key_ids:
            query_keyword = DBHandler().table_select_where("keywords", ["keyword"], {"ID": str(key_id[0])} )
            cloud.add_keyword(query_keyword[0][0], key_id[0])
        self.current_keywords = list(cloud.get_current_keywords().keys())
        # add keyword lineEdit and selector
        info_layout.addWidget(KeywordAdd(cloud))
        info_layout.addWidget(KeywordSelector(cloud))
        info_layout.addWidget(cloud)

    def collect_data(self):
        data_layout = self.scroll_widget.layout()
        dataset = {}
        new_keywords = None
        for i in range(1, data_layout.count()):
            data_element  = data_layout.itemAt(i).widget()
            if isinstance(data_element, SingleDataElement) is True:
                element_key =  DBStructure.db_elements[data_element.get_id()].title
                dataset[element_key] = data_layout.itemAt(i).widget().get_text()
            if isinstance(data_element, KeywordCloud) is True:
                new_keywords = list(data_element.get_current_keywords().keys())
        dataset[DBStructure.doctype.title] = self.type
        dataset[DBStructure.ID.title] = self.doc_id
        return dataset, self.current_keywords, new_keywords, self.token

