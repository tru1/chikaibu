import configparser, os

class Configs:
    def create():
        config = configparser.ConfigParser()
        working_directory = str(os.getcwd()).replace("\\", "/")
        default_file_path = working_directory + "/files"
        config['files'] = {'path': default_file_path}
        config['dark_gray'] = {'red': '18', 'green': '18', 'blue': '18'}
        config['gray01'] = {'red': '28', 'green': '28', 'blue': '28'}
        config['gray02'] = {'red': '33', 'green': '33', 'blue': '33'}
        config['gray03'] = {'red': '35', 'green': '35', 'blue': '35'}
        config['gray04'] = {'red': '38', 'green': '38', 'blue': '38'}
        config['gray06'] = {'red': '44', 'green': '44', 'blue': '44'}
        config['gray08'] = {'red': '45', 'green': '45', 'blue': '45'}
        config['gray12'] = {'red': '50', 'green': '50', 'blue': '50'}
        config['gray16'] = {'red': '51', 'green': '51', 'blue': '51'}
        config['gray24'] = {'red': '55', 'green': '55', 'blue': '55'}
        config['accent1'] = {'red': '230', 'green': '243', 'blue': '229'}
        config['accent2'] = {'red': '194', 'green': '225', 'blue': '192'}
        config['accent3'] = {'red': '155', 'green': '206', 'blue': '152'}
        config['accent4'] = {'red': '115', 'green': '188', 'blue': '112'}
        config['accent5'] = {'red': '87', 'green': '174', 'blue': '84'}
        config['text1'] = {'red': '227', 'green': '227', 'blue': '227'}
        config['text2'] = {'red': '157', 'green': '157', 'blue': '157'}
        config['text3'] = {'red': '107', 'green': '107', 'blue': '107'}
        with open('config.ini', 'w') as config_file:
            config.write(config_file)

    def get_color(section):
        if not os.path.isfile('config.ini'):
            Configs.create()
        config = configparser.ConfigParser()
        config.read('config.ini')
        return config.get(section, 'red'), config.get(section, 'green'), config.get(section, 'blue')

    def get_file_path():
        if not os.path.isfile('config.ini'):
            Configs.create()
        config = configparser.ConfigParser()
        config.read('config.ini')
        return config.get('files', 'path')

    def set(section, key, value):
        if not os.path.isfile('config.ini'):
            Configs.create()
        parser = configparser.ConfigParser()
        parser.read('config.ini')
        parser.set(str(section), str(key), value)
        with open("config.ini", 'w') as config_file:
            parser.write(config_file)

