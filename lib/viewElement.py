from lib.helpers import *
from lib.metaData import metaHandler


class ViewElement(QWidget):
    element_data = None

    def __init__(self, parent):
        super(ViewElement, self).__init__(parent)

    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(Colors.dp01)
        painter.drawRoundedRect(8, 10, self.width()-16, self.height()-20, 10.0, 10.0)

    def set_view_id(self, element_id):
        self.element_data = DBHandler().table_select_where("documents", ["*"], {"ID": element_id} )[0]
        QWidget().setLayout(self.layout())
        new_layout = QHBoxLayout(self)
        new_layout.setContentsMargins(15, 8, 15, 8)
        new_layout.addWidget(DataView(self, self.element_data))
        new_layout.addSpacing(15)
        note_widget = QWidget()
        note_layout = QVBoxLayout(note_widget)
        note_layout.addSpacing(13)
        title_label = CustomLabel(self, Colors.text_medium, 16, True, "Notes")
        note_layout.addWidget(title_label, 0, Qt.AlignCenter)
        note_layout.addWidget(NoteView(self, False, self.element_data[DBStructure.notes.id]))
        new_layout.addWidget(note_widget)

    def get_data(self):
        return self.element_data

class DataView(QScrollArea):
    def __init__(self, parent, element_data):
        super(DataView, self).__init__(parent)
        self.setFixedWidth(600)
        self.setFrameStyle(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)
        # create main widget in scroll area
        scroll_widget = QWidget(self)
        pal = QPalette()
        pal.setColor(scroll_widget.backgroundRole(), Colors.dp01)
        scroll_widget.setPalette(pal)
        self.setWidget(scroll_widget)
        info_layout = QVBoxLayout(scroll_widget)
        info_layout.setContentsMargins(20, 20, 20, 20)
        info_layout.setAlignment(Qt.AlignTop)
        info_layout.setSpacing(20)

        # create title
        title_widget = QWidget()
        title_layout = QHBoxLayout(title_widget)
        pic = QLabel()
        pic.setFixedWidth(60)
        title_label = QLabel()
        if element_data[DBStructure.doctype.id] == "article":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("article.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Article Info")
        elif element_data[DBStructure.doctype.id] == "book":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("book.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Book Info")
        elif element_data[DBStructure.doctype.id] == "thesis":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("thesis.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Thesis Info")
        elif element_data[DBStructure.doctype.id] == "other":
            pic.setPixmap(Helpers.changeImageColor(QImage(Helpers.resource_path("other.png")), Colors.green200))
            title_label = CustomLabel(title_widget, Colors.text_high, 16, True, "Other Info")
        title_layout.addWidget(pic)
        title_layout.addWidget(title_label)
        # add button for citation count, only for article type         
        if element_data[DBStructure.doctype.id] == "article":
            self.citation_button = QPushButton()
            self.citation_button.setText("get citation count")
            self.citation_button.setObjectName("bulk")
            new_font = QApplication.font()
            new_font.setPointSize(11)
            new_font.setBold(True)
            new_font.setPointSize(12)
            self.citation_button.setFont(new_font)
            self.doi = element_data[DBStructure.doi.id]
            self.citation_button.clicked.connect(lambda: self.get_citation())
            self.citation_button.setFixedSize(170, 30)
            title_layout.addSpacing(15)
            title_layout.addWidget(self.citation_button)
        info_layout.addWidget(title_widget, 0, Qt.AlignCenter)

        # create info content
        elements = [item for item in DBStructure.db_elements if element_data[DBStructure.doctype.id] in item.block]
        for x in elements:
            info_layout.addWidget(SingleDataElement(self, x.title, str(element_data[x.id]), False, x.id))
        info_layout.addSpacing(10)
        info_layout.addWidget(HLine(Colors.dp08, 2, 2))
        info_layout.addSpacing(10)
        # add keyword view
        query_key_ids = DBHandler().table_select_where("documentKeywords", ["keywordID"], {"documentID": str(element_data[DBStructure.ID.id])} )
        cloud = KeywordCloud(parent, False)
        for key_id in query_key_ids:
            query_keyword = DBHandler().table_select_where("keywords", ["keyword"], {"ID": str(key_id[0])} )
            cloud.add_keyword(query_keyword[0][0], key_id[0])
        info_layout.addWidget(cloud)

    def get_citation(self):
        self.citation_button.setText("cited by: "+str(metaHandler.citation_count(self.doi)))
        self.citation_button.setEnabled(False)

