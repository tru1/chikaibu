from crossref_commons.retrieval import get_publication_as_json as get_meta_data
import arxiv


class metaHandler:

    @staticmethod
    def create_token(item_data):
        token = ""
        try:
            author = str(item_data["author"][0]["family"]).lower()
            special_chars = {' ':'', 'ä':'ae', 'ö':'oe', 'ü':'ue', 'ê':'e', 'é':'e', 'è':'e'}
            for char in special_chars:            
                author = author.replace(char, special_chars[char])
            token = token + author
            token = token + "-"
        except:
            token = token + ""       
        try:
            token = token + str(item_data["published-print"]["date-parts"][0][0])
        except:
            token = token + ""       
        return token

    @staticmethod
    def create_authors(item_data):
        author_string = ""
        try:
            for author in item_data["author"]:
                if author_string != "":
                    author_string = author_string + " and "
                author_string = author_string + author["family"]
                author_string = author_string + ", "
                author_string = author_string + author["given"]
        except:
             author_string= author_string + ""       
        return author_string

    @staticmethod
    def citation_count(doi):
        item = []
        successful = True
        try:
            item  = get_meta_data(doi)
        except:
            successful = False
        return(item['is-referenced-by-count'])

    @staticmethod
    def get_article_meta(doi):
        item = []
        successful = True
        try:
            item  = get_meta_data(doi)
        except:
            successful = False
        meta_data = { }
        meta_data['Token'] = metaHandler.create_token(item)
        meta_data['Authors'] = metaHandler.create_authors(item)
        try:
            meta_data['Title'] = item["title"][0]
        except:
            meta_data['Title'] = ""
        try:
            meta_data['Year'] = item["published-print"]["date-parts"][0][0]
        except:
            meta_data['Year'] = ""
        try:
            meta_data['Journal'] = item["container-title"][0]
        except:
            meta_data['Journal'] = ""
        try:
            meta_data['Volume'] = item["volume"]
        except:
            meta_data['Volume'] = ""
        try:
            meta_data['Number'] = item["issue"]
        except:
            meta_data['Number'] = ""
        try:
            meta_data['Pages'] = item["page"]
        except:
            meta_data['Pages'] = ""
        try:
            meta_data['DOI'] = item["DOI"]
        except:
            meta_data['DOI'] = ""
        meta_data["Note"] = ""
        return meta_data, successful

    @staticmethod
    def get_arxiv_article_meta(ar_id):
        item = []
        successful = True
        ar_id = str(ar_id).replace("arXiv:", "")
        try:
            item  = arxiv.query(id_list=[ar_id])
        except:
            successful = False
        meta_data = { }
        try:
            arxiv_authors = item[0]["authors"]
        except:
            arxiv_authors = []
        # generate authors string in bibtex format
        author_list_tmp = ""
        for full_name in arxiv_authors:
            if author_list_tmp != "":
                author_list_tmp = author_list_tmp + " and "
            author_list_tmp = author_list_tmp + str(full_name.split()[-1]) + ", "
            for i in range(len(full_name.split())-1):
                author_list_tmp = author_list_tmp + (full_name.split())[i]
                if i is not len(full_name.split())-2:
                    author_list_tmp = author_list_tmp + " "
        meta_data['Authors'] = author_list_tmp
        try:
            meta_data['Title'] = str(item[0]["title"]).replace('\n ', '')
        except:
            meta_data['Title'] = ""
        try:
            meta_data['Year'] = item[0]["published_parsed"].tm_year
        except:
            meta_data['Year'] = ""
        token_author = str((arxiv_authors[0].split())[-1]).lower()
        special_chars = {' ':'', 'ä':'ae', 'ö':'oe', 'ü':'ue', 'ê':'e', 'é':'e', 'è':'e'}
        for char in special_chars:            
            token_author = token_author.replace(char, special_chars[char])
        meta_data['Token'] = token_author + "-" + str(meta_data['Year'])
        try:
            meta_data['Journal'] = item[0]["journal_reference"]
        except:
            meta_data['Journal'] = ""
        if meta_data['Journal'] is None:
            meta_data['Journal'] = "arXiv"
        meta_data['Volume'] = ""
        meta_data['Number'] = ""
        meta_data['Pages'] = ""
        try:
            meta_data['DOI'] = item[0]["doi"]
        except:
            meta_data['DOI'] = ""
        if meta_data['DOI'] is None:
            meta_data['DOI'] = "arXiv:"+str(ar_id)
        meta_data["Note"] = ""
        return meta_data, successful

    @staticmethod
    def get_book_meta(doi):
        item = []
        successful = True
        try:
            item  = get_meta_data(doi)
        except:
            successful = False
        meta_data = { }
        meta_data['Token'] = metaHandler.create_token(item)
        meta_data['Authors'] = metaHandler.create_authors(item)
        try:
            meta_data['Title'] = item["title"][0]
        except:
            meta_data['Title'] = ""
        try:
            meta_data['Year'] = item["published-print"]["date-parts"][0][0]
        except:
            meta_data['Year'] = ""
        try:
            meta_data['DOI'] = item["DOI"]
        except:
            meta_data['DOI'] = ""
        try:
            meta_data['Publisher'] = item["publisher"]
        except:
            meta_data['Publisher'] = ""
        try:
            meta_data['ISBN'] = item["isbn-type"][0]["value"]
        except:
            meta_data['ISBN'] = ""
        try:
            meta_data['Series'] = item["container-title"][0]
        except:
            meta_data['Series'] = ""
        meta_data["Volume"] = ""
        meta_data["Address"] = ""
        meta_data["Note"] = ""
        return meta_data, successful

