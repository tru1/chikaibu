from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from functools import partial
from lib.db import DBHandler, DBStructure, FileHandler
from lib.config import *
import sys


page_id = {
    "library": 0,
    "add_element": 1,
    "view_element": 2,                                                                                                                                          "settings": 3,                                                                                                                                              "edit_element": 4                                                                                                                                       }   

class Colors:
    # shades of base color
    color = Configs.get_color('dark_gray')
    dark_gray = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('gray01')
    dp01 = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('gray08')
    dp08 = QColor(int(color[0]), int(color[1]), int(color[2]))
    # accent colors
    color = Configs.get_color('accent1')
    green50 = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('accent3')
    green200 = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('accent5')
    green400 = QColor(int(color[0]), int(color[1]), int(color[2]))
    # text colors
    color = Configs.get_color('text1')
    text_high = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('text2')
    text_medium = QColor(int(color[0]), int(color[1]), int(color[2]))
    color = Configs.get_color('text3')
    text_disabled = QColor(int(color[0]), int(color[1]), int(color[2]))

    def str_from_color(color):
        return str(color.red())+", "+str(color.green())+", "+str(color.blue())


class Helpers:
    def resource_path(relative_path):
        if hasattr(sys, '_MEIPASS'):
            return os.path.join(sys._MEIPASS, relative_path)
        return os.path.join(os.path.abspath("img/"), relative_path)

    def changeImageColor(image, to_color):
        image = image.alphaChannel()
        for i in range(0,image.colorCount()):
            to_color.setAlpha(qGray(image.color(i)))
            image.setColor(i,to_color.rgba())
        new_pixmap = QPixmap.fromImage(image)
        return new_pixmap
        
    def imageToColorGradient(image, color_1, color_2, ratio_1, ratio_2):
        result = QPixmap(image.size())
        result.fill(Qt.transparent)
        pix1 = QPixmap.fromImage(image)
        pix2 = QPixmap(image.size())
        
        painter = QPainter(pix2)
        painter.setRenderHint(QPainter.Antialiasing)

        gradient = QLinearGradient(0, 0, image.width(), image.height())
        gradient.setColorAt(ratio_1, color_1)
        gradient.setColorAt(ratio_2, color_2)
        brush = QBrush(gradient)
        painter.setBrush(brush)
        painter.setPen(Qt.NoPen)

        painter.drawRect(image.rect())
        painter.end()

        painter.begin(result)
        painter.drawPixmap(image.rect(), pix1)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.drawPixmap(image.rect(), pix2)
        painter.end()
        return result

    def check_completeness(self, data_input, old_token = None):
        # check for cpulicate token
        if DBHandler().check_for_value("documents", "token", data_input[DBStructure.token.title]):
            if data_input[DBStructure.token.title] != old_token:
                ErrorBox("Duplicate", "The given token already exists in the database")
                return False
        # check if common elements are filled in
        common_elements = [element for element in DBStructure.db_elements if element.title in "Token,Authors,Title,Year,Type"]
        for elem in common_elements:
            if data_input[elem.title] == "" or data_input[elem.title] is None:
                ErrorBox("Missing data", "Please insert a "+elem.title)
                return False
        # check doctype specific data elements
        if data_input[DBStructure.doctype.title] == "article":
            if data_input[DBStructure.journal.title] == "" or data_input[DBStructure.journal.title] is None:
                ErrorBox("Missing data", "Please insert a "+DBStructure.journal.title)
                return False
        elif data_input[DBStructure.doctype.title] == "book":
            if data_input[DBStructure.publisher.title] == "" or data_input[DBStructure.publisher.title] is None:
                ErrorBox("Missing data", "Please insert a "+DBStructure.publisher.title)
                return False
        elif data_input[DBStructure.doctype.title] == "thesis":
            if data_input[DBStructure.school.title] == "" or data_input[DBStructure.school.title] is None:
                ErrorBox("Missing data", "Please insert a "+DBStructure.school.title)
                return False
            elif data_input[DBStructure.thesis_type.title] == "" or data_input[DBStructure.thesis_type.title] is None:
                ErrorBox("Missing data", "Please insert a "+DBStructure.thesis_type.title)
                return False
        return True


class BasicFlowLayout(QLayout):
    def __init__(self, parent):
        super(BasicFlowLayout, self).__init__(parent)
        self.setContentsMargins(0, 0, 0, 0)
        self.setSpacing(0)
        self.widget_list = []

    def __del__(self):
        for i in range(0,len(self.widget_list)):
            self.widget_list.pop(0)

    def addItem(self, widget):
        self.widget_list.append(widget)

    def count(self):
        return len(self.widget_list)

    def itemAt(self, index):
        if index >= 0 and index < len(self.widget_list):
            return self.widget_list[index]
        return None

    def takeAt(self, index):
        if index >= 0 and index < len(self.widget_list):
            return self.widget_list.pop(index)
        return None

    def setGeometry(self, rect):
        super(BasicFlowLayout, self).setGeometry(rect)
        self.doLayout(rect, False)

    def sizeHint(self):
        return self.minimumSize()

    def minimumSize(self):
        width = self.geometry().width()
        minWidth, minHeight = self.doLayout(QRect(0, 0, width, 0), True)
        return QSize(minWidth, minHeight)

    def doLayout(self, rect, testOnly):
        x = rect.x()
        y = rect.y()
        hSpacing = 10
        vSpacing = 8
        lineHeight = 0
        if self.widget_list:
            lineHeight = self.widget_list[0].widget().sizeHint().height()
        maxLineWidth = 0
        # position single widgets  
        for item in self.widget_list:
            if x + item.sizeHint().width() > rect.right() and lineHeight > 0:
                maxLineWidth = max(x-rect.x(), maxLineWidth, item.sizeHint().width())
                x = rect.x()
                y = y + lineHeight + vSpacing
                lineHeight = 0
            nextX = x + item.sizeHint().width() + hSpacing
            if not testOnly:
                item.setGeometry(QRect(QPoint(x, y), item.sizeHint()))
            x = nextX
            lineHeight = max(lineHeight, item.sizeHint().height())

        return maxLineWidth, y + lineHeight - rect.y()


class KeywordAdd(QWidget):
    def __init__(self, key_cloud):
        super(KeywordAdd, self).__init__()
        self.cloud = key_cloud
        layout = QHBoxLayout(self)
        # add keyword label
        label = CustomLabel(self, Colors.text_high, 12, True, "Add new keyword ", 160)
        layout.addWidget(label)
        # lineEdit to enter new keyword
        self.edit = QLineEdit(self)
        font12 = QApplication.font()
        font12.setPointSize(12)
        self.edit.setFont(font12)
        self.edit.home(False)
        layout.addWidget(self.edit)
        # button to add new keyword to sqlite database
        add_button = QPushButton(self)
        font18 = QApplication.font()
        font18.setPointSize(18)
        font18.setBold(True)
        add_button.setFont(font18)
        add_button.setFixedWidth(30)
        add_button.setText("+")
        add_button.setObjectName("addDelete")
        add_button.clicked.connect(lambda: self.add_keyword(self.edit.text()))
        layout.addWidget(add_button)

    def add_keyword(self, word):
        if not DBHandler().check_for_value("keywords", "keyword", word):
            keywordset = {"keyword": word}
            new_id = DBHandler().add_data("keywords", keywordset)
            self.cloud.add_keyword(word, new_id)
            self.edit.setText("")
        else:
            ErrorBox("Duplicate", "The given keyword already exists in the database")


class KeywordSelector(QWidget):
    def __init__(self, key_cloud):
        super(KeywordSelector, self).__init__()
        self.cloud = key_cloud
        layout = QHBoxLayout(self)
        # fetch data from keyword table and create dropdown menu
        key_table = DBHandler().get_table("keywords")
        self.selector = QComboBox(self)
        font10 = QApplication.font()
        font10.setPointSize(10)
        self.selector.setFont(font10)
        self.selector.addItem(" - ", -1)
        layout.addWidget(self.selector)
        self.fill_in_items()
        # add selected keyword to related cloud
        self.selector.currentIndexChanged.connect(lambda: self.select_event())

    def setCurrentIndex(self, index):
        self.selector.setCurrentIndex(index)

    def select_event(self):
        self.cloud.add_keyword(self.selector.itemText(self.selector.currentIndex()),
                              self.selector.itemData(self.selector.currentIndex()))
        self.selector.setCurrentIndex(0)

    def fill_in_items(self):
        key_dict = {}
        key_table = DBHandler().get_table("keywords")
        for i in range(0,len(key_table)):
            key_dict[key_table[i][1]] = key_table[i][0]
        for keyword in sorted(key_dict, key=str.casefold):
            self.selector.addItem(str(keyword), key_dict[keyword])

    def refresh(self):
        for i in range(1,self.selector.count()):
            self.selector.removeItem(1)
        self.fill_in_items()


class KeywordCloud(QWidget):
    keywordChanged = Signal()

    def __init__(self, parent, deletable=True):
        super(KeywordCloud, self).__init__(parent)
        self.current_key_dict = {}
        self.layout = BasicFlowLayout(self)
        self.deletable = deletable
        self.parent = parent

    def add_keyword(self, word, word_id):
        if word_id >= 0 and word_id not in self.current_key_dict:
            self.current_key_dict[word_id] = word
            self.layout.addWidget(KeywordElement(self, self.parent.width()-20, word, word_id, self.deletable))
            self.keywordChanged.emit()

    def purge_cloud(self):
        key_list = list(self.current_key_dict)
        for key in key_list:
            self.remove_keyword(key)

    def remove_keyword(self, word_id):
        # remove keyword from current list
        try:
            del self.current_key_dict[word_id]
        except KeyError:
            print("ERROR: Key "+str(word_id)+" not found when trying to delete from KeywordCloud")
        # remove KeywordElement object from the cloud
        items = (self.layout.itemAt(i).widget() for i in range(self.layout.count()))
        for w in items:
            if isinstance(w, KeywordElement) is True and w.get_keyword_id() == word_id:
                w.deleteLater()
        self.keywordChanged.emit()

    def get_current_keywords(self):
        return self.current_key_dict


class KeywordElement(QFrame):
    def __init__(self, parent, size, text, key_id=-1, deletable=True):
        super(KeywordElement, self).__init__()
        self.keyword_id = key_id
        key_elem_layout = QHBoxLayout(self)
        key_elem_layout.setContentsMargins(10, 0, 0, 0)
        key_elem_layout.setSpacing(0)
        self.setObjectName("KeywordElement")
        font = QApplication.font()
        font.setPointSize(12)
        metrics = QFontMetrics(font)
        if deletable is True:
            size_shift = 50
        else:
            size_shift = 30
        # Keyword label
        max_text_size = size - size_shift
        key_label = QLabel()
        key_label.setFont(font)
        elided = metrics.elidedText(str(text), Qt.ElideRight, max_text_size)
        key_label.setText(elided)
        key_label.setObjectName("keyword")
        key_elem_layout.addWidget(key_label)
        if deletable is True:
            # button to delete from current keyword-cloud
            delete_label = QPushButton(self)
            font.setBold(True)
            delete_label.setFont(font)
            delete_label.setFixedWidth(30)
            delete_label.setText("X")
            delete_label.setObjectName("addDelete")
            delete_label.clicked.connect(lambda: parent.remove_keyword(key_id))
            key_elem_layout.addWidget(delete_label)
        text_size = min(max_text_size, metrics.boundingRect(text).width())
        self.setFixedWidth(text_size+size_shift)

    def get_keyword_id(self):
        return self.keyword_id


class ErrorBox(QMessageBox):
    def __init__(self, title, text):
        super(ErrorBox, self).__init__()
        self.setIcon(QMessageBox.Warning)
        self.setWindowTitle(title)
        self.setText(text)
        self.setStandardButtons(QMessageBox.Close)
        self.exec_()


class HLine(QFrame):
    def __init__(self, line_color, height, width):
        super(HLine, self).__init__()
        self.setFixedHeight(height)
        self.setLineWidth(width)
        self.setStyleSheet(f"border:none; background:rgb({line_color.red()},{line_color.green()},{line_color.blue()});")
        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Plain)


class CustomLabel(QLabel):
    def __init__(self, parent, color, size, bold, text, width=None):
        super(CustomLabel, self).__init__(parent)
        if text == "None":
            text = " - "
        current_font = QApplication.font()
        current_font.setBold(bold)
        current_font.setPointSize(size)
        self.setFont(current_font)
        self.setText(text)
        self.setStyleSheet("color: rgb(" + Colors.str_from_color(color) + "); background-color: transparent;")
        if width is not None:
            if width == 0:
                width = self.width()
            self.setFixedWidth(width)


class NoteView(QScrollArea):
    def __init__(self, parent, editable, note_data=None):
        super(NoteView, self).__init__(parent)
        self.setFrameStyle(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)
        # create main widget in scroll area
        scroll_widget = QWidget(self)
        pal = QPalette()
        pal.setColor(scroll_widget.backgroundRole(), Colors.dp01)
        scroll_widget.setPalette(pal)
        self.setWidget(scroll_widget)
        notes_layout = QVBoxLayout(scroll_widget)
        notes_layout.setAlignment(Qt.AlignTop)
        font12 = QApplication.font()
        font12.setPointSize(12)
        # add content
        if editable:
            self.note_widget = QTextEdit()
            self.note_widget.setFont(font12)
            if note_data is not None:
                self.note_widget.setPlainText(note_data)
        else:
            self.note_widget = QTextEdit()
            self.note_widget.setFont(font12)
            self.note_widget.setObjectName("view")
            if note_data is not None and note_data != "":
                self.note_widget.setMarkdown(note_data)
            self.note_widget.setReadOnly(True)
        notes_layout.addWidget(self.note_widget)

    def get_text(self):
        text = self.note_widget.toPlainText()
        return text

    def refresh(self):
        self.note_widget.setText("")


class ProgressBar(QProgressBar):
    def __init__(self, value):
        super(ProgressBar, self).__init__()
        self.setValue(value)
        self.setFixedHeight(15) 
        font = QApplication.font()
        font.setPointSize(8)
        font.setBold(True)
        self.setFont(font)


class SingleDataElement(QWidget):
    def __init__(self, parent, name_text, data_text, editable=False, field_id=-1):
        super(SingleDataElement, self).__init__(parent)
        self.id = field_id
        element_layout = QHBoxLayout(self)
        element_layout.setContentsMargins(0, 0, 0, 0)
        element_layout.setAlignment(Qt.AlignLeft)
        self.name_label = CustomLabel(self, Colors.text_high, 12, True, name_text)
        self.name_label.setFixedWidth(120)
        if editable:
            self.data_label = QLineEdit(self)
            self.data_label.setFixedWidth(parent.width()-self.name_label.width()-50)
            font12 = QApplication.font()
            font12.setPointSize(12)
            self.data_label.setFont(font12)
            self.data_label.setText(data_text)
            self.data_label.home(False)
            element_layout.addWidget(self.name_label, 0, Qt.AlignCenter)
        else:
            self.data_label = CustomLabel(self, Colors.text_high, 12, False, data_text)
            self.data_label.setFixedWidth(parent.width()-self.name_label.width()-50)
            self.data_label.setWordWrap(True)
            self.data_label.setTextInteractionFlags(Qt.TextSelectableByMouse)
            element_layout.addWidget(self.name_label, 0, Qt.AlignTop)
        element_layout.addWidget(self.data_label)

    def get_text(self):
        return self.data_label.text()

    def set_text(self, text):
        self.data_label.setText(text)
        self.data_label.home(False)

    def get_name(self):
        return self.name_label.text()

    def get_id(self):
        return self.id


class DropZone(QWidget):
    dropped = False
    file_path = None

    def __init__(self, parent):
        super(DropZone, self).__init__(parent)
        self.parent = parent
        self.setAcceptDrops(True)
        self.setFixedWidth(350)
        self.setFixedHeight(100)

    def paintEvent(self, ev):
        painter = QPainter(self)
        painter.setRenderHint(painter.Antialiasing)
        font = QApplication.font()
        font.setPointSize(22)
        font.setBold(True)
        painter.setFont(font)
        if self.dropped:
            painter.setBrush(Colors.dp01)
            pen = QPen(Colors.green200, 3, Qt.SolidLine)
            painter.setPen(pen)
            painter.drawRoundedRect(3, 3, self.width()-6, self.height()-6, 15.0, 15.0)
            painter.setPen(Colors.green200)
            painter.drawText(80, 60, "File Accepted")
        else:
            painter.setBrush(Colors.dp01)
            pen = QPen(Colors.dp08, 3, Qt.SolidLine)
            painter.setPen(pen)
            painter.drawRoundedRect(3, 3, self.width()-6, self.height()-6, 15.0, 15.0)
            painter.setPen(Colors.text_high)
            painter.drawText(40, 60, "Drop PDF File here")

    def pageIndexChanged(self):
        self.dropped = False
        self.file_path = None

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        self.dropped = True
        self.repaint()
        if e.mimeData().hasUrls:
            e.setDropAction(Qt.CopyAction)
            e.accept()
            for url in e.mimeData().urls():
                file_name = str(url.toLocalFile())
                self.file_path = file_name
        else:
            e.ignore()

    def get_file_path(self):
        return self.file_path


class BibFile:
    def generate(self, element_list):
        bibtex = ""
        special_character = {"&":"\&", "ß": "{\ss}", "ä": """{\\"a}""", "Ä": """{\\"A}""", "ö": """{\\"o}""",
                             "Ö": """{\\"O}""", "ü": """{\\"u}""", "Ü": """{\\"U}"""}
        for element in element_list:
            if element[DBStructure.doctype.id] == "article":
                bibtex = bibtex +  "@article{"+element[DBStructure.token.id]+",\n"
            elif element[DBStructure.doctype.id] == "book":
                bibtex = bibtex + "@book{"+element[DBStructure.token.id]+",\n"
            elif element[DBStructure.doctype.id] == "thesis":
                bibtex = bibtex + "@masterthesis{"+element[DBStructure.token.id]+",\n"
            elif element[DBStructure.doctype.id] == "other":
                bibtex = bibtex + "@misc{"+element[DBStructure.token.id]+",\n"

            data_fields = [field for field in DBStructure.db_elements if element[DBStructure.doctype.id] in field.block]
            for item in data_fields:
                if item.bibtex != "none" and element[item.id] != "" and element[item.id] is not None:
                    content = str(element[item.id])
                    for char in special_character:
                        content = content.replace(char, special_character[char])
                    bibtex = bibtex + item.bibtex + "= \"" + content + "\",\n"
            bibtex = bibtex[:-2]
            bibtex = bibtex + "\n}\n\n"
        return bibtex

