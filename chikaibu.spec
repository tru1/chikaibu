# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['chikaibu.py'],
             pathex=['D:\\gitReposSim\\chikaibu'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
a.datas += [ ('addElem.png', '.\\img\\addElem.png', 'DATA')]
a.datas += [ ('article.png', '.\\img\\article.png', 'DATA')]
a.datas += [ ('back.png', '.\\img\\back.png', 'DATA')]
a.datas += [ ('bib_clip.png', '.\\img\\bib_clip.png', 'DATA')]
a.datas += [ ('bib_file.png', '.\\img\\bib_file.png', 'DATA')]
a.datas += [ ('book.png', '.\\img\\book.png', 'DATA')]
a.datas += [ ('delElem.png', '.\\img\\delElem.png', 'DATA')]
a.datas += [ ('edit.png', '.\\img\\edit.png', 'DATA')]
a.datas += [ ('folder.png', '.\\img\\folder.png', 'DATA')]
a.datas += [ ('icon.png', '.\\img\\icon.png', 'DATA')]
a.datas += [ ('icon16.png', '.\\img\\icon16.png', 'DATA')]
a.datas += [ ('icon24.png', '.\\img\\icon24.png', 'DATA')]
a.datas += [ ('icon32.png', '.\\img\\icon32.png', 'DATA')]
a.datas += [ ('icon48.png', '.\\img\\icon48.png', 'DATA')]
a.datas += [ ('link.png', '.\\img\\link.png', 'DATA')]
a.datas += [ ('loupe.png', '.\\img\\loupe.png', 'DATA')]
a.datas += [ ('other.png', '.\\img\\other.png', 'DATA')]
a.datas += [ ('pdf.png', '.\\img\\pdf.png', 'DATA')]
a.datas += [ ('save.png', '.\\img\\save.png', 'DATA')]
a.datas += [ ('settings.png', '.\\img\\settings.png', 'DATA')]
a.datas += [ ('thesis.png', '.\\img\\thesis.png', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='chikaibu',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
