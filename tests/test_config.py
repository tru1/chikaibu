from lib.config import *

def test_confCreate():
    Configs.create()
    test = Configs.get_file_path()
    exists = os.path.isfile(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert exists == True
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')

def test_confColor():
    Configs.create()
    gray1 = Configs.get_color('gray01')
    gray2 = Configs.get_color('gray16')
    accent = Configs.get_color('accent2')
    text = Configs.get_color('text2')
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert gray1 == ('28', '28', '28')
    assert gray2 == ('51', '51', '51')
    assert accent == ('194', '225', '192')
    assert text == ('157', '157', '157')

def test_confFilePath_1():
    Configs.get_file_path()
    exists = os.path.isfile(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert exists == True
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')

def test_confFilePath_2():
    file_path = Configs.get_file_path()
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert file_path == str(os.getcwd()).replace("\\", "/") + "/files"

def test_confSetElement_1():
    Configs.set('accent1', 'red', '230')
    exists = os.path.isfile(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert exists == True
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')

def test_confSetElement_2():
    value1 = Configs.get_color('dark_gray')
    Configs.set('dark_gray', 'blue', '123')
    value2 = Configs.get_color('dark_gray')
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert value1[2] == '18'
    assert value2[2] == '123'

def test_confSetElement_3():
    value1 = Configs.get_color('accent1')
    Configs.set('accent1', 'red', '123')
    value2 = Configs.get_color('accent1')
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert value1[0] == '230'
    assert value2[0] == '123'

def test_confSetElement_4():
    value1 = Configs.get_color('text1')
    Configs.set('text1', 'green', '123')
    value2 = Configs.get_color('text1')
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert value1[1] == '227'
    assert value2[1] == '123'

def test_confSetElement_5():
    value1 = Configs.get_file_path()
    Configs.set('files', 'path', 'blubb')
    value2 = Configs.get_file_path()
    os.remove(str(os.getcwd()).replace("\\", "/")+'/config.ini')
    assert value1 == str(os.getcwd()).replace("\\", "/") + "/files"
    assert value2 == 'blubb'

