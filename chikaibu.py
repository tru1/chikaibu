import sys, ctypes
from content import *
from menu import *
from style import stylesheet

# Python Package Requirements (3rd party)
# - pyperclip
# - PySide2
# - crossref_commons
# Python Package Requirements (python base package)
# - os, sys, ctypes
# - configparser, shutil
# - subprocess, collections
# - functools, sqlite
# Name from the japanese
#  -> Chie = Wisdom
#  -> Akaibu = Archiv

class MasterWindow(QMainWindow):
    def __init__(self):
        super(MasterWindow, self).__init__(None)
        self.setCentralWidget(MasterFrame(self))
        self.setMinimumHeight(770)
        self.setMinimumWidth(1200)
        self.resize(1200, 770)

    def resizeEvent(self, event):
        p = QPalette()
        gradient = QLinearGradient(0, 0, event.size().height(), event.size().width())
        gradient.setColorAt(0.3, Colors.dark_gray)
        gradient.setColorAt(0.95, Colors.dp01)
        p.setBrush(QPalette.Window, QBrush(gradient))
        self.setPalette(p)


class MasterFrame(QWidget):
    def __init__(self, parent):
        super(MasterFrame, self).__init__(parent)
        self.window = parent
        self.setContentsMargins(0, 0, 0, 0)
        library_widget = Library(self)
        add_element_widget = AddElement(self)
        view_element_widget = ViewElement(self)
        edit_element_widget = EditElement(self)
        self.content_frame = QStackedWidget()
        self.content_frame.insertWidget(page_id["library"], library_widget)
        self.content_frame.insertWidget(page_id["add_element"], add_element_widget)
        self.content_frame.insertWidget(page_id["view_element"], view_element_widget)
        self.setting_page = SettingsPage(self)
        self.content_frame.insertWidget(page_id["settings"], self.setting_page)
        self.content_frame.insertWidget(page_id["edit_element"], edit_element_widget)
        self.content_frame.setCurrentIndex(page_id["library"])
        self.content_frame.currentChanged.connect(add_element_widget.refresh)
        self.menu_frame = QStackedWidget()
        self.menu_frame.setFixedHeight(130)
        add_menu = AddMenu(self, add_element_widget)
        edit_menu = EditMenu(self, edit_element_widget)
        self.menu_frame.insertWidget(page_id["library"], LibraryMenu(self, library_widget))
        self.menu_frame.insertWidget(page_id["add_element"], add_menu)
        self.menu_frame.insertWidget(page_id["view_element"], ViewMenu(self, view_element_widget))
        self.menu_frame.insertWidget(page_id["settings"], SettingsMenu(self))
        self.menu_frame.insertWidget(page_id["edit_element"], edit_menu)
        self.menu_frame.setCurrentIndex(page_id["library"])
        self.menu_frame.currentChanged.connect(add_menu.refresh)

        main_layout = QGridLayout(self)
        main_layout.setHorizontalSpacing(10)
        main_layout.setVerticalSpacing(10)
        main_layout.addWidget(LogoFrame(self),0,0)
        main_layout.addWidget(self.menu_frame,0,1)
        self.sidebar_frame = SideBarFrame(self)
        main_layout.addWidget(self.sidebar_frame,1,0)
        main_layout.addWidget(self.content_frame,1,1)

    def switch(self, i, view_id, ev=None):
        self.content_frame.setCurrentIndex(i)
        self.menu_frame.setCurrentIndex(i)
        if view_id is not None:
            try:
                self.content_frame.widget(i).set_view_id(view_id)
            except:
                print("ERROR: In set_view_id or function not defined or page id "+str(i)+" with view id"+str(view_id))
        if i == page_id["library"]:
            self.sidebar_frame.update_statistics()
        if i == page_id["settings"]:
            self.setting_page.set_keyword_table()

    def reset_lib_view(self):
        if self.content_frame.currentIndex() == page_id["library"]:
            self.sidebar_frame.update_statistics()
            self.content_frame.widget(page_id["library"]).reset_list_and_search()


class LogoFrame(QWidget):
    def __init__(self, parent):
        super(LogoFrame, self).__init__(parent)
        self.setFixedHeight(130)
        self.setFixedWidth(150)
        logo_layout = QVBoxLayout(self)
        logo_layout.setAlignment(Qt.AlignCenter)
        logo_layout.setContentsMargins(0, 0, 0, 0)
        pic = QLabel(self)
        logo_image = QImage(Helpers.resource_path("icon.png"))
        logo_pixmap = Helpers.imageToColorGradient(logo_image, Colors.green50, Colors.green400, 0.1, 0.75)
        pic.setPixmap(logo_pixmap)
        pic.setStyleSheet("background-color: transparent;")
        logo_layout.addWidget(pic)


class SideBarFrame(QWidget):
    def __init__(self, parent):
        super(SideBarFrame, self).__init__(parent)
        self.setFixedWidth(150)
        bar_layout = QVBoxLayout(self)
        bar_layout.setContentsMargins(0, 0, 0, 15)
        bar_layout.setSpacing(0)
        bar_layout.setAlignment(Qt.AlignBottom)

        # add bars
        self.article_bar = ProgressBar(0)
        bar_layout.addWidget(self.article_bar)
        article_label = CustomLabel(self, Colors.text_disabled, 10, False, "Articles")
        bar_layout.addWidget(article_label)
        bar_layout.addSpacing(15)
        self.book_bar = ProgressBar(0)
        bar_layout.addWidget(self.book_bar)
        book_label = CustomLabel(self, Colors.text_disabled, 10, False, "Books")
        bar_layout.addWidget(book_label)
        bar_layout.addSpacing(15)
        self.thesis_bar = ProgressBar(0)
        bar_layout.addWidget(self.thesis_bar)
        thesis_label = CustomLabel(self, Colors.text_disabled, 10, False, "Theses")
        bar_layout.addWidget(thesis_label)
        bar_layout.addSpacing(15)
        self.other_bar = ProgressBar(0)
        bar_layout.addWidget(self.other_bar)
        other_label = CustomLabel(self, Colors.text_disabled, 10, False, "Others")
        bar_layout.addWidget(other_label)
        bar_layout.addSpacing(40)
        # total item count
        counter_label = CustomLabel(self, Colors.text_disabled, 14, True, "Total number:")
        bar_layout.addWidget(counter_label, 0, Qt.AlignLeft)
        self.counter_label = CustomLabel(self, Colors.text_disabled, 14, True, "   "+str(0))
        bar_layout.addWidget(self.counter_label, 0, Qt.AlignCenter)
        bar_layout.addSpacing(30)
        bar_layout.addWidget(HLine(Colors.dp08, 2, 2))
        bar_layout.addSpacing(30)
        self.update_statistics()

        settings_button = QWidget(self)
        settings_button.setFixedHeight(35)
        setting_button_layout = QHBoxLayout(settings_button)
        setting_button_layout.setContentsMargins(5, 0, 0, 0)
        setting_pic_widget = QLabel()
        setting_pic_widget.setFixedWidth(35)
        setting_pic_widget.setFixedHeight(35)
        setting_image = QImage(Helpers.resource_path("settings.png"))
        setting_image = setting_image.scaled(30, 30, Qt.KeepAspectRatio)
        setting_pixmap = Helpers.changeImageColor(setting_image, Colors.text_medium)
        setting_pic_widget.setPixmap(setting_pixmap)
        setting_pic_widget.setStyleSheet("background-color: transparent;")
        setting_text = CustomLabel(self, Colors.text_medium, 12, False, "Settings")
        setting_text.setFixedHeight(30)
        setting_button_layout.addWidget(setting_pic_widget)
        setting_button_layout.addWidget(setting_text)
        settings_button.mousePressEvent = partial(parent.switch, page_id["settings"], None)
        bar_layout.addWidget(settings_button, 0, Qt.AlignCenter)

    def update_statistics(self):
        # get data for statistics
        number_docs = len(DBHandler().get_table("documents"))
        number_articles = len(DBHandler().table_select_where("documents", ["*"], {DBStructure.doctype.title: "article"}))
        number_book = len(DBHandler().table_select_where("documents", ["*"], {DBStructure.doctype.title: "book"}))
        number_thesis = len(DBHandler().table_select_where("documents", ["*"], {DBStructure.doctype.title: "thesis"}))
        number_other = len(DBHandler().table_select_where("documents", ["*"], {DBStructure.doctype.title: "other"}))
        # update values
        if number_docs == 0:
            number_docs = 0.1
        self.article_bar.setValue(100*number_articles/number_docs)
        self.book_bar.setValue(100*number_book/number_docs)
        self.thesis_bar.setValue(100*number_thesis/number_docs)
        self.other_bar.setValue(100*number_other/number_docs)
        self.counter_label.setText("   "+str(number_docs))

if __name__ == '__main__':
    # delete unused keywords
    keyword_table = DBHandler().get_table("keywords")
    for keyword in keyword_table:
      contained = DBHandler().check_for_value("documentKeywords", "keywordID", str(keyword[0]))
      if contained is False:
          DBHandler().delete_entry("keywords", {"ID": keyword[0]})
    # main app
    #ctypes.windll.shcore.SetProcessDpiAwareness(0)
    app = QApplication(sys.argv)
    app.setStyleSheet(stylesheet)
    mw = MasterWindow()
    mw.setWindowTitle("Chikaibu")
    app_icon = QIcon()
    app_icon.addFile(Helpers.resource_path('icon16.png'), QSize(16, 16))
    app_icon.addFile(Helpers.resource_path('icon24.png'), QSize(16, 16))
    app_icon.addFile(Helpers.resource_path('icon32.png'), QSize(32, 23))
    app_icon.addFile(Helpers.resource_path('icon48.png'), QSize(48, 48))
    app.setWindowIcon(app_icon)
    mw.show()
    sys.exit(app.exec_())

